﻿using Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxDoor
{
    public partial class AnularVenta : Form
    {
        public AnularVenta()
        {
            InitializeComponent();
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Anularbutton_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("Desea anular la venta seleccionada?", "", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Inventarios inv = new Inventarios();
                Ventas ventas = new Ventas();
                DataTable dtDetalle = ventas.ListadoVentaDetalle(Convert.ToInt32(VentasdataGridView.CurrentRow.Cells[0].Value));


                for (int i = 0; i < dtDetalle.Rows.Count; i++)
                {
                    inv.ActualizarInventario((int)dtDetalle.Rows[i]["IdProducto"], "+", (double)dtDetalle.Rows[i]["Cantidad"]);
                }
                if (ventas.AnularVenta(Convert.ToInt32(VentasdataGridView.CurrentRow.Cells[0].Value)))
                {
                    ventas.AnularDetalleVenta(Convert.ToInt32(VentasdataGridView.CurrentRow.Cells[0].Value));
                }
                MessageBox.Show("Venta anulada satisfactoriamente");
                CargarVentas();
            }

        }

        private void AnularVenta_Load(object sender, EventArgs e)
        {
            CargarVentas();
        }

        public void CargarVentas()
        {
            Ventas ventas = new Ventas();

            VentasdataGridView.DataSource = ventas.ListadoAnularVentas();
            VentasdataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            VentasdataGridView.Columns[0].Width = 60;
            VentasdataGridView.Columns[1].Width = 130;
            VentasdataGridView.Columns[3].Width = 130;
            VentasdataGridView.Columns[4].Width = 160;

            VentasdataGridView.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            VentasdataGridView.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            VentasdataGridView.Columns[3].DefaultCellStyle.Format = "N2";
            VentasdataGridView.Columns[4].DefaultCellStyle.Format = "N2";
        }
    }
}
