﻿namespace MaxDoor
{
    partial class BusquedaGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FiltrotextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ResultadosdataGridView = new System.Windows.Forms.DataGridView();
            this.Seleccionarbutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ResultadosdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // FiltrotextBox
            // 
            this.FiltrotextBox.Location = new System.Drawing.Point(12, 77);
            this.FiltrotextBox.Name = "FiltrotextBox";
            this.FiltrotextBox.Size = new System.Drawing.Size(1035, 27);
            this.FiltrotextBox.TabIndex = 0;
            this.FiltrotextBox.TextChanged += new System.EventHandler(this.FiltrotextBox_TextChanged);
            this.FiltrotextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FiltrotextBox_KeyDown);
            this.FiltrotextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FiltrotextBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Busqueda";
            // 
            // ResultadosdataGridView
            // 
            this.ResultadosdataGridView.AllowUserToAddRows = false;
            this.ResultadosdataGridView.AllowUserToDeleteRows = false;
            this.ResultadosdataGridView.AllowUserToResizeColumns = false;
            this.ResultadosdataGridView.AllowUserToResizeRows = false;
            this.ResultadosdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ResultadosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ResultadosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ResultadosdataGridView.Location = new System.Drawing.Point(12, 110);
            this.ResultadosdataGridView.Name = "ResultadosdataGridView";
            this.ResultadosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ResultadosdataGridView.Size = new System.Drawing.Size(1035, 458);
            this.ResultadosdataGridView.TabIndex = 2;
            this.ResultadosdataGridView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ResultadosdataGridView_KeyPress);
            // 
            // Seleccionarbutton
            // 
            this.Seleccionarbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Seleccionarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Seleccionarbutton.ForeColor = System.Drawing.Color.White;
            this.Seleccionarbutton.Location = new System.Drawing.Point(911, 574);
            this.Seleccionarbutton.Name = "Seleccionarbutton";
            this.Seleccionarbutton.Size = new System.Drawing.Size(136, 42);
            this.Seleccionarbutton.TabIndex = 10;
            this.Seleccionarbutton.Text = "Seleccionar";
            this.Seleccionarbutton.UseVisualStyleBackColor = false;
            this.Seleccionarbutton.Click += new System.EventHandler(this.Seleccionarbutton_Click);
            // 
            // BusquedaGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1059, 628);
            this.Controls.Add(this.Seleccionarbutton);
            this.Controls.Add(this.ResultadosdataGridView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FiltrotextBox);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BusquedaGeneral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BusquedaGeneral";
            this.Load += new System.EventHandler(this.BusquedaGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ResultadosdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox FiltrotextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView ResultadosdataGridView;
        private System.Windows.Forms.Button Seleccionarbutton;
    }
}