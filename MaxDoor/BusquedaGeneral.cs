﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor
{
    public partial class BusquedaGeneral : Form
    {
        public static int Identificador;

        public BusquedaGeneral()
        {
            InitializeComponent();
            ResultadosdataGridView.RowHeadersVisible = false;
        }

        private void BusquedaGeneral_Load(object sender, EventArgs e)
        {
            LlenarGrid();
            ResultadosdataGridView.SelectedRows[0].Selected = false;
            //if(ResultadosdataGridView.Rows.Count > 0)
            //{
            //    ResultadosdataGridView.CurrentCell = ResultadosdataGridView.Rows[0].Cells[1];
            //}

        }

        public void LlenarGrid()
        {
            if(Identificador == 1)
            {
                Clientes clientes = new Clientes();
                ResultadosdataGridView.DataSource = clientes.Listado(" IdCliente, Nombres, Apellidos, Cedula, Telefono ", " Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if(Identificador == 2)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if (Identificador == 3)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
        }

        public void Filtrar()
        {
            if (Identificador == 1)
            {
                Clientes clientes = new Clientes();
                ResultadosdataGridView.DataSource = clientes.Listado(" IdCliente, Nombres, Apellidos, Cedula, Telefono ", " (Nombres like '%"+ FiltrotextBox.Text + "%' or Apellidos like '%" + FiltrotextBox.Text + "%' or Cedula like '%" + FiltrotextBox.Text + "%') and Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if(Identificador == 2)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Nombre like '%"+ FiltrotextBox.Text +"%' and Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if (Identificador == 3)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Nombre like '%" + FiltrotextBox.Text + "%' and Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
        }

        private void FiltrotextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void FiltrotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                {
                    if (Identificador == 1)
                    {
                        RetornarCliente();
                    }
                    else if (Identificador == 2)
                    {
                        RetornarProductos();
                    }
                    else if (Identificador == 3)
                    {
                        RetornarProductosCompras();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void FiltrotextBox_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void ResultadosdataGridView_KeyPress(object sender, KeyPressEventArgs e)
        {
            //try
            //{
            //    if (Identificador == 1)
            //    {
            //        RetornarCliente();
            //    }
            //    else if (Identificador == 2)
            //    {
            //        RetornarProductos();
            //    }
            //    else if (Identificador == 3)
            //    {
            //        RetornarProductosCompras();
            //    }
            //}              
            //catch (Exception)
            //{

            //}
        }

        public void RetornarCliente()
        {
            Registros.RegistroVentas.IdCliente = Convert.ToInt32(ResultadosdataGridView.CurrentRow.Cells[0].Value.ToString());
            Registros.RegistroVentas.NombreCliente = ResultadosdataGridView.CurrentRow.Cells[1].Value.ToString() + " " + ResultadosdataGridView.CurrentRow.Cells[2].Value.ToString();
            this.Close();
        }

        public void RetornarProductos()
        {
            Registros.RegistroVentas.InfoProducto[0] = ResultadosdataGridView.CurrentRow.Cells[0].Value.ToString();
            Registros.RegistroVentas.InfoProducto[1] = ResultadosdataGridView.CurrentRow.Cells[1].Value.ToString();
            Registros.RegistroVentas.InfoProducto[2] = ResultadosdataGridView.CurrentRow.Cells[2].Value.ToString();
            this.Close();
        }

        public void RetornarProductosCompras()
        {
            Registros.RegistroCompras.InfoProducto[0] = ResultadosdataGridView.CurrentRow.Cells[0].Value.ToString();
            Registros.RegistroCompras.InfoProducto[1] = ResultadosdataGridView.CurrentRow.Cells[1].Value.ToString();
            Registros.RegistroCompras.InfoProducto[2] = ResultadosdataGridView.CurrentRow.Cells[2].Value.ToString();
            this.Close();
        }

        private void Seleccionarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Identificador == 1)
                {
                    RetornarCliente();
                }
                else if (Identificador == 2)
                {
                    RetornarProductos();
                }
                else if (Identificador == 3)
                {
                    RetornarProductosCompras();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
