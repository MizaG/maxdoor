﻿namespace MaxDoor
{
    partial class ListadoGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Listadolabel = new System.Windows.Forms.Label();
            this.ResultadosdataGridView = new System.Windows.Forms.DataGridView();
            this.FiltrotextBox = new System.Windows.Forms.TextBox();
            this.Okbutton = new System.Windows.Forms.Button();
            this.Nuevobutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ResultadosdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Listadolabel
            // 
            this.Listadolabel.AutoSize = true;
            this.Listadolabel.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Listadolabel.Location = new System.Drawing.Point(12, 9);
            this.Listadolabel.Name = "Listadolabel";
            this.Listadolabel.Size = new System.Drawing.Size(74, 22);
            this.Listadolabel.TabIndex = 0;
            this.Listadolabel.Text = "Listado";
            // 
            // ResultadosdataGridView
            // 
            this.ResultadosdataGridView.AllowUserToAddRows = false;
            this.ResultadosdataGridView.AllowUserToDeleteRows = false;
            this.ResultadosdataGridView.AllowUserToResizeColumns = false;
            this.ResultadosdataGridView.AllowUserToResizeRows = false;
            this.ResultadosdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ResultadosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ResultadosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ResultadosdataGridView.Location = new System.Drawing.Point(12, 137);
            this.ResultadosdataGridView.Name = "ResultadosdataGridView";
            this.ResultadosdataGridView.RowHeadersVisible = false;
            this.ResultadosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ResultadosdataGridView.Size = new System.Drawing.Size(978, 423);
            this.ResultadosdataGridView.TabIndex = 4;
            this.ResultadosdataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ResultadosdataGridView_CellDoubleClick);
            // 
            // FiltrotextBox
            // 
            this.FiltrotextBox.Location = new System.Drawing.Point(12, 104);
            this.FiltrotextBox.Name = "FiltrotextBox";
            this.FiltrotextBox.Size = new System.Drawing.Size(978, 27);
            this.FiltrotextBox.TabIndex = 3;
            this.FiltrotextBox.TextChanged += new System.EventHandler(this.FiltrotextBox_TextChanged);
            // 
            // Okbutton
            // 
            this.Okbutton.BackColor = System.Drawing.Color.Transparent;
            this.Okbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Okbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Okbutton.ForeColor = System.Drawing.Color.Black;
            this.Okbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Okbutton.Location = new System.Drawing.Point(895, 566);
            this.Okbutton.Name = "Okbutton";
            this.Okbutton.Size = new System.Drawing.Size(95, 50);
            this.Okbutton.TabIndex = 48;
            this.Okbutton.Text = "O.K.";
            this.Okbutton.UseVisualStyleBackColor = false;
            this.Okbutton.Click += new System.EventHandler(this.Okbutton_Click);
            // 
            // Nuevobutton
            // 
            this.Nuevobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Nuevobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Nuevobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nuevobutton.ForeColor = System.Drawing.Color.White;
            this.Nuevobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Nuevobutton.Location = new System.Drawing.Point(794, 566);
            this.Nuevobutton.Name = "Nuevobutton";
            this.Nuevobutton.Size = new System.Drawing.Size(95, 50);
            this.Nuevobutton.TabIndex = 49;
            this.Nuevobutton.Text = "Nuevo";
            this.Nuevobutton.UseVisualStyleBackColor = false;
            this.Nuevobutton.Click += new System.EventHandler(this.Nuevobutton_Click);
            // 
            // ListadoGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1002, 623);
            this.Controls.Add(this.Nuevobutton);
            this.Controls.Add(this.Okbutton);
            this.Controls.Add(this.ResultadosdataGridView);
            this.Controls.Add(this.FiltrotextBox);
            this.Controls.Add(this.Listadolabel);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "ListadoGeneral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListadoGeneral";
            this.Load += new System.EventHandler(this.ListadoGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ResultadosdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Listadolabel;
        private System.Windows.Forms.DataGridView ResultadosdataGridView;
        private System.Windows.Forms.TextBox FiltrotextBox;
        private System.Windows.Forms.Button Okbutton;
        private System.Windows.Forms.Button Nuevobutton;
    }
}