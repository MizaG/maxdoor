﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor
{
    public partial class ListadoGeneral : Form
    {
        public static int Identificador;
        public ListadoGeneral()
        {
            InitializeComponent();
        }

        public void LlenarGrid()
        {
            if (Identificador == 1)
            {
                Clientes clientes = new Clientes();
                ResultadosdataGridView.DataSource = clientes.Listado(" IdCliente, Nombres, Apellidos, Cedula, Telefono ", " Activo = 1 ", "");
                Listadolabel.Text = "Listado de Clientes";
            }
            else if (Identificador == 2)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Activo = 1 ", "");
                Listadolabel.Text = "Listado de Productos";
            }
            else if (Identificador == 3)
            {
                Proveedores proveedores = new Proveedores();
                ResultadosdataGridView.DataSource = proveedores.Listado(" IdProveedor, Empresa, Representante, Telefono, Celuluar as 'Celular', Correo", " Activo = 1 ", "");
                Listadolabel.Text = "Listado de Proveedores";
            }
            else if (Identificador == 4)
            {
                Usuarios usuarios = new Usuarios();
                ResultadosdataGridView.DataSource = usuarios.Listado(" IdUsuario, Nombres, Apellidos, NombreUsuario ", " Activo = 1 ", "");
            }
            else if (Identificador == 5)
            {
                Compras compras = new Compras();
                ResultadosdataGridView.DataSource = compras.ListadoCompras(FiltrotextBox.Text);
            }
            ResultadosdataGridView.Columns[0].Visible = false;
        }

        private void Okbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListadoGeneral_Load(object sender, EventArgs e)
        {
            LlenarGrid();
        }

        public void Filtrar()
        {
            if (Identificador == 1)
            {
                Clientes clientes = new Clientes();
                ResultadosdataGridView.DataSource = clientes.Listado(" IdCliente, Nombres, Apellidos, Cedula, Telefono ", " (Nombres like '%" + FiltrotextBox.Text + "%' or Apellidos like '%" + FiltrotextBox.Text + "%' or Cedula like '%" + FiltrotextBox.Text + "%') and Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if (Identificador == 2)
            {
                Productos productos = new Productos();
                ResultadosdataGridView.DataSource = productos.Listado(" IdProducto, Nombre, Precio, Costo ", " Nombre like '%" + FiltrotextBox.Text + "%' and Activo = 1 ", "");
                ResultadosdataGridView.Columns[0].Visible = false;
            }
            else if (Identificador == 3)
            {
                Proveedores proveedores = new Proveedores();
                ResultadosdataGridView.DataSource = proveedores.Listado(" IdProveedor, Empresa, Representante, RNC, Direccion, Telefono, Celular, Correo ", " (Empresa like '%" + FiltrotextBox + "%' or Representante like '" + FiltrotextBox + "') and Activo = 1 ", "");
            }
            else if (Identificador == 4)
            {
                Usuarios usuarios = new Usuarios();
                ResultadosdataGridView.DataSource = usuarios.Listado(" IdUsuario, Nombres, Apellidos, NombreUsuario ", "(Nombres like '%" + FiltrotextBox.Text + "%' or Apellidos like '%" + FiltrotextBox.Text + "%' or NombreUsuario like '%" + FiltrotextBox.Text + "%') and Activo = 1  ", "");
            }
            else if(Identificador == 5)
            {
                Compras compras = new Compras();
                ResultadosdataGridView.DataSource = compras.ListadoCompras(FiltrotextBox.Text);
            }

        }

        private void FiltrotextBox_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        private void Nuevobutton_Click(object sender, EventArgs e)
        {
            if (Identificador == 1)
            {
                Registros.RegistroCliente registroCliente = new Registros.RegistroCliente();
                registroCliente.ShowDialog();
                LlenarGrid();
            }
            else if (Identificador == 2)
            {
                Registros.RegistroProductos registroProductos = new Registros.RegistroProductos();
                registroProductos.ShowDialog();
                LlenarGrid();
            }
            else if(Identificador == 3)
            {
                Registros.RegistroProveedores registroProveedores = new Registros.RegistroProveedores();
                registroProveedores.ShowDialog();
                LlenarGrid();
            }
            else if(Identificador == 4)
            {
                Registros.RegistroUsuario registroUsuario = new Registros.RegistroUsuario();
                registroUsuario.ShowDialog();
                LlenarGrid();
            }
            else if(Identificador == 5)
            {
                Registros.RegistroCompras registroCompras = new Registros.RegistroCompras();
                registroCompras.ShowDialog();
                LlenarGrid();
            }
        }

        private void ResultadosdataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Identificador == 1)
            {
                Registros.RegistroCliente.IdEdicion = Convert.ToInt32(ResultadosdataGridView.CurrentRow.Cells[0].Value);
                Registros.RegistroCliente registroCliente = new Registros.RegistroCliente();
                registroCliente.ShowDialog();
                Registros.RegistroCliente.IdEdicion = 0;
                LlenarGrid();
            }
            else if (Identificador == 2)
            {
                Registros.RegistroProductos.IdEdicion = Convert.ToInt32(ResultadosdataGridView.CurrentRow.Cells[0].Value);
                Registros.RegistroProductos registroProductos = new Registros.RegistroProductos();
                registroProductos.ShowDialog();
                Registros.RegistroProductos.IdEdicion = 0;
                LlenarGrid();
            }
            else if(Identificador == 3)
            {
                Registros.RegistroProveedores.IdEdicion = Convert.ToInt32(ResultadosdataGridView.CurrentRow.Cells[0].Value);
                Registros.RegistroProveedores registroProveedores = new Registros.RegistroProveedores();
                registroProveedores.ShowDialog();
                Registros.RegistroProveedores.IdEdicion = 0;
                LlenarGrid();
                
            }
            else if(Identificador == 4)
            {
                Registros.RegistroUsuario.IdEdicion = Convert.ToInt32(ResultadosdataGridView.CurrentRow.Cells[0].Value);
                Registros.RegistroUsuario registroUsuario = new Registros.RegistroUsuario();
                registroUsuario.ShowDialog();
                Registros.RegistroUsuario.IdEdicion = 0;
                LlenarGrid();
            }
        }
    }
}
