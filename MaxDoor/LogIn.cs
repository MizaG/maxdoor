﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor
{
    public partial class LogIn : Form
    {
        public static int IdUsuario;
        public static int Rol;
        public static string NombreUsuario;
        public LogIn()
        {
            InitializeComponent();
        }

        private void Salirbutton_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void Iniciarbutton_Click(object sender, EventArgs e)
        {
            Usuarios usuarios = new Usuarios();
            if(usuarios.Buscar(UsuariotextBox.Text, PasstextBox.Text))
            {
                IdUsuario = usuarios.IdUsuario;
                Rol = usuarios.Rol;
                NombreUsuario = usuarios.NombreUsuario;

                Principal principal = new Principal();
                //Process.Start("C:\\Users\\mizadlo garcia\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
                principal.Show();
                this.Hide();
                
                
            }
            else
            {
                MessageBox.Show("Usuario y/o Contraseña incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
