﻿namespace MaxDoor
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.BarraVerticalpanel = new System.Windows.Forms.Panel();
            this.Pagosbutton = new System.Windows.Forms.Button();
            this.Cientesbutton = new System.Windows.Forms.Button();
            this.Ventabutton = new System.Windows.Forms.Button();
            this.Salirbutton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ContenederPrincipalpanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PagosHoydataGridView = new System.Windows.Forms.DataGridView();
            this.AtrasodataGridView = new System.Windows.Forms.DataGridView();
            this.ProxPagosdataGridView = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pagosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anularToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BarraVerticalpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ContenederPrincipalpanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PagosHoydataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AtrasodataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxPagosdataGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BarraVerticalpanel
            // 
            this.BarraVerticalpanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.BarraVerticalpanel.Controls.Add(this.Pagosbutton);
            this.BarraVerticalpanel.Controls.Add(this.Cientesbutton);
            this.BarraVerticalpanel.Controls.Add(this.Ventabutton);
            this.BarraVerticalpanel.Controls.Add(this.Salirbutton);
            this.BarraVerticalpanel.Controls.Add(this.pictureBox1);
            this.BarraVerticalpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.BarraVerticalpanel.Location = new System.Drawing.Point(0, 0);
            this.BarraVerticalpanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BarraVerticalpanel.Name = "BarraVerticalpanel";
            this.BarraVerticalpanel.Size = new System.Drawing.Size(233, 571);
            this.BarraVerticalpanel.TabIndex = 0;
            // 
            // Pagosbutton
            // 
            this.Pagosbutton.FlatAppearance.BorderSize = 0;
            this.Pagosbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pagosbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pagosbutton.ForeColor = System.Drawing.Color.White;
            this.Pagosbutton.Image = ((System.Drawing.Image)(resources.GetObject("Pagosbutton.Image")));
            this.Pagosbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Pagosbutton.Location = new System.Drawing.Point(12, 307);
            this.Pagosbutton.Name = "Pagosbutton";
            this.Pagosbutton.Size = new System.Drawing.Size(214, 62);
            this.Pagosbutton.TabIndex = 15;
            this.Pagosbutton.Text = "Pagos";
            this.Pagosbutton.UseVisualStyleBackColor = true;
            this.Pagosbutton.Click += new System.EventHandler(this.Pagosbutton_Click);
            // 
            // Cientesbutton
            // 
            this.Cientesbutton.FlatAppearance.BorderSize = 0;
            this.Cientesbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cientesbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cientesbutton.ForeColor = System.Drawing.Color.White;
            this.Cientesbutton.Image = ((System.Drawing.Image)(resources.GetObject("Cientesbutton.Image")));
            this.Cientesbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cientesbutton.Location = new System.Drawing.Point(12, 171);
            this.Cientesbutton.Name = "Cientesbutton";
            this.Cientesbutton.Size = new System.Drawing.Size(214, 62);
            this.Cientesbutton.TabIndex = 14;
            this.Cientesbutton.Text = "Clientes";
            this.Cientesbutton.UseVisualStyleBackColor = true;
            this.Cientesbutton.Click += new System.EventHandler(this.Cientesbutton_Click);
            // 
            // Ventabutton
            // 
            this.Ventabutton.FlatAppearance.BorderSize = 0;
            this.Ventabutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ventabutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ventabutton.ForeColor = System.Drawing.Color.White;
            this.Ventabutton.Image = ((System.Drawing.Image)(resources.GetObject("Ventabutton.Image")));
            this.Ventabutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Ventabutton.Location = new System.Drawing.Point(12, 239);
            this.Ventabutton.Name = "Ventabutton";
            this.Ventabutton.Size = new System.Drawing.Size(214, 62);
            this.Ventabutton.TabIndex = 13;
            this.Ventabutton.Text = "Ventas";
            this.Ventabutton.UseVisualStyleBackColor = true;
            this.Ventabutton.Click += new System.EventHandler(this.Ventabutton_Click);
            // 
            // Salirbutton
            // 
            this.Salirbutton.FlatAppearance.BorderSize = 0;
            this.Salirbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Salirbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Salirbutton.ForeColor = System.Drawing.Color.White;
            this.Salirbutton.Image = ((System.Drawing.Image)(resources.GetObject("Salirbutton.Image")));
            this.Salirbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Salirbutton.Location = new System.Drawing.Point(12, 375);
            this.Salirbutton.Name = "Salirbutton";
            this.Salirbutton.Size = new System.Drawing.Size(214, 62);
            this.Salirbutton.TabIndex = 12;
            this.Salirbutton.Text = "Salir";
            this.Salirbutton.UseVisualStyleBackColor = true;
            this.Salirbutton.Click += new System.EventHandler(this.Salirbutton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 9);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ContenederPrincipalpanel
            // 
            this.ContenederPrincipalpanel.Controls.Add(this.tableLayoutPanel1);
            this.ContenederPrincipalpanel.Controls.Add(this.menuStrip1);
            this.ContenederPrincipalpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContenederPrincipalpanel.Location = new System.Drawing.Point(233, 0);
            this.ContenederPrincipalpanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ContenederPrincipalpanel.Name = "ContenederPrincipalpanel";
            this.ContenederPrincipalpanel.Size = new System.Drawing.Size(726, 571);
            this.ContenederPrincipalpanel.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.PagosHoydataGridView, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.AtrasodataGridView, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.ProxPagosdataGridView, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 571);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(720, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Dashboard Administrativo";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(720, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pagos pendiente a la Fecha";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(720, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Atraso";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 385);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(720, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Proximos Pagos";
            // 
            // PagosHoydataGridView
            // 
            this.PagosHoydataGridView.AllowUserToAddRows = false;
            this.PagosHoydataGridView.AllowUserToDeleteRows = false;
            this.PagosHoydataGridView.AllowUserToResizeColumns = false;
            this.PagosHoydataGridView.AllowUserToResizeRows = false;
            this.PagosHoydataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PagosHoydataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PagosHoydataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PagosHoydataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.PagosHoydataGridView.Location = new System.Drawing.Point(3, 47);
            this.PagosHoydataGridView.Name = "PagosHoydataGridView";
            this.PagosHoydataGridView.RowHeadersVisible = false;
            this.PagosHoydataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PagosHoydataGridView.Size = new System.Drawing.Size(720, 153);
            this.PagosHoydataGridView.TabIndex = 3;
            // 
            // AtrasodataGridView
            // 
            this.AtrasodataGridView.AllowUserToAddRows = false;
            this.AtrasodataGridView.AllowUserToDeleteRows = false;
            this.AtrasodataGridView.AllowUserToResizeColumns = false;
            this.AtrasodataGridView.AllowUserToResizeRows = false;
            this.AtrasodataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AtrasodataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AtrasodataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AtrasodataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.AtrasodataGridView.Location = new System.Drawing.Point(3, 228);
            this.AtrasodataGridView.Name = "AtrasodataGridView";
            this.AtrasodataGridView.RowHeadersVisible = false;
            this.AtrasodataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AtrasodataGridView.Size = new System.Drawing.Size(720, 153);
            this.AtrasodataGridView.TabIndex = 4;
            // 
            // ProxPagosdataGridView
            // 
            this.ProxPagosdataGridView.AllowUserToAddRows = false;
            this.ProxPagosdataGridView.AllowUserToDeleteRows = false;
            this.ProxPagosdataGridView.AllowUserToResizeColumns = false;
            this.ProxPagosdataGridView.AllowUserToResizeRows = false;
            this.ProxPagosdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ProxPagosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProxPagosdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProxPagosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProxPagosdataGridView.Location = new System.Drawing.Point(3, 409);
            this.ProxPagosdataGridView.Name = "ProxPagosdataGridView";
            this.ProxPagosdataGridView.RowHeadersVisible = false;
            this.ProxPagosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProxPagosdataGridView.Size = new System.Drawing.Size(720, 159);
            this.ProxPagosdataGridView.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.comprasToolStripMenuItem,
            this.inventarioToolStripMenuItem,
            this.productosToolStripMenuItem,
            this.proveedoresToolStripMenuItem,
            this.reportesToolStripMenuItem,
            this.anularToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(726, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.comprasToolStripMenuItem.Text = "Compras";
            this.comprasToolStripMenuItem.Click += new System.EventHandler(this.comprasToolStripMenuItem_Click);
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.inventarioToolStripMenuItem.Text = "Inventario";
            this.inventarioToolStripMenuItem.Click += new System.EventHandler(this.inventarioToolStripMenuItem_Click);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.productosToolStripMenuItem.Text = "Productos";
            this.productosToolStripMenuItem.Click += new System.EventHandler(this.productosToolStripMenuItem_Click);
            // 
            // proveedoresToolStripMenuItem
            // 
            this.proveedoresToolStripMenuItem.Name = "proveedoresToolStripMenuItem";
            this.proveedoresToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.proveedoresToolStripMenuItem.Text = "Proveedores";
            this.proveedoresToolStripMenuItem.Click += new System.EventHandler(this.proveedoresToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasToolStripMenuItem1,
            this.pagosToolStripMenuItem});
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // ventasToolStripMenuItem1
            // 
            this.ventasToolStripMenuItem1.Name = "ventasToolStripMenuItem1";
            this.ventasToolStripMenuItem1.Size = new System.Drawing.Size(108, 22);
            this.ventasToolStripMenuItem1.Text = "Ventas";
            this.ventasToolStripMenuItem1.Click += new System.EventHandler(this.ventasToolStripMenuItem1_Click);
            // 
            // pagosToolStripMenuItem
            // 
            this.pagosToolStripMenuItem.Name = "pagosToolStripMenuItem";
            this.pagosToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.pagosToolStripMenuItem.Text = "Pagos";
            this.pagosToolStripMenuItem.Click += new System.EventHandler(this.pagosToolStripMenuItem_Click);
            // 
            // anularToolStripMenuItem
            // 
            this.anularToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ventasToolStripMenuItem});
            this.anularToolStripMenuItem.Name = "anularToolStripMenuItem";
            this.anularToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.anularToolStripMenuItem.Text = "Anular";
            // 
            // ventasToolStripMenuItem
            // 
            this.ventasToolStripMenuItem.Name = "ventasToolStripMenuItem";
            this.ventasToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.ventasToolStripMenuItem.Text = "Ventas";
            this.ventasToolStripMenuItem.Click += new System.EventHandler(this.ventasToolStripMenuItem_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(959, 571);
            this.Controls.Add(this.ContenederPrincipalpanel);
            this.Controls.Add(this.BarraVerticalpanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Principal";
            this.Text = "Max Door";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Principal_Load);
            this.BarraVerticalpanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ContenederPrincipalpanel.ResumeLayout(false);
            this.ContenederPrincipalpanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PagosHoydataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AtrasodataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProxPagosdataGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BarraVerticalpanel;
        private System.Windows.Forms.Panel ContenederPrincipalpanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView PagosHoydataGridView;
        private System.Windows.Forms.DataGridView AtrasodataGridView;
        private System.Windows.Forms.DataGridView ProxPagosdataGridView;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Ventabutton;
        private System.Windows.Forms.Button Salirbutton;
        private System.Windows.Forms.Button Cientesbutton;
        private System.Windows.Forms.Button Pagosbutton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proveedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anularToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pagosToolStripMenuItem;
    }
}

