﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor
{
    public partial class Principal : Form
    {

        public Principal()
        {
            InitializeComponent();
        }

        private void Cientesbutton_Click(object sender, EventArgs e)
        {
            ListadoGeneral.Identificador = 1;
            ListadoGeneral listadoGeneral = new ListadoGeneral();
            listadoGeneral.ShowDialog();
        }

        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListadoGeneral.Identificador = 3;
            ListadoGeneral listadoGeneral = new ListadoGeneral();
            listadoGeneral.ShowDialog();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListadoGeneral.Identificador = 2;
            ListadoGeneral listadoGeneral = new ListadoGeneral();
            listadoGeneral.ShowDialog();
        }

        private void Ventabutton_Click(object sender, EventArgs e)
        {
            Registros.RegistroVentas registroVentas = new Registros.RegistroVentas();
            registroVentas.ShowDialog();
        }

        private void Pagosbutton_Click(object sender, EventArgs e)
        {
            Registros.RegistroPagos registroPagos = new Registros.RegistroPagos();
            registroPagos.ShowDialog();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListadoGeneral.Identificador = 4;
            ListadoGeneral listadoGeneral = new ListadoGeneral();
            listadoGeneral.ShowDialog();
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Registros.RegistroInventario registroInventario = new Registros.RegistroInventario();
            registroInventario.ShowDialog();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            if(LogIn.Rol == 0)
            {
                menuStrip1.Visible = true;
            }

            Cuotas cuotas = new Cuotas();
            PagosHoydataGridView.DataSource = cuotas.ListadoDashboard(" and cast(c.FechaCuota as date)  = cast(GETDATE() as date) order by FechaCuota ; ");
            AtrasodataGridView.DataSource = cuotas.ListadoDashboard(" and cast(c.FechaCuota as date)  < cast(GETDATE() as date) order by FechaCuota desc;");
            ProxPagosdataGridView.DataSource = cuotas.ListadoDashboard(" and DATEDIFF(day, GETDATE(), c.FechaCuota) = 1 order by FechaCuota ;");
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListadoGeneral.Identificador = 5;
            ListadoGeneral listadoGeneral = new ListadoGeneral();
            listadoGeneral.ShowDialog();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnularVenta anularVenta = new AnularVenta();
            anularVenta.ShowDialog();
        }

        private void ventasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VentanaReportes.VentanaReporteVentas ventanaReporteVentas = new VentanaReportes.VentanaReporteVentas();
            ventanaReporteVentas.ShowDialog();
        }

        private void pagosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VentanaReportes.VentaReportePagos ventaReportePagos = new VentanaReportes.VentaReportePagos();
            ventaReportePagos.ShowDialog();
        }

        private void Salirbutton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
