﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class CantidadInventario : Form
    {
        public static bool Sumar;
        public static int IdProducto;
        Inventarios inventarios;
        
        public CantidadInventario()
        {
            InitializeComponent();
            inventarios = new Inventarios();
        }

        public bool LlenarDatoInventario()
        {
            try
            {
                if (IdProducto > 0)
                {
                    inventarios.IdProducto = IdProducto;
                    inventarios.Existencia = Convert.ToInt32(CantidadtextBox.Text);
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void GuardarInventario()
        {
            try
            {
                if (!inventarios.Buscar(IdProducto))
                {
                    if (LlenarDatoInventario())
                    {
                        if (inventarios.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Error al Guardar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan datos");
                    }
                }
                else
                {
                    int cantidad = Convert.ToInt32(CantidadtextBox.Text);

                    if (Convert.ToInt32(CantidadtextBox.Text) > 0)
                    {
                        if (Sumar)
                        {
                            CantidadtextBox.Text = (cantidad + inventarios.Existencia).ToString();
                        }
                        else
                        {
                            CantidadtextBox.Text = (inventarios.Existencia - cantidad).ToString();
                        }

                        if (LlenarDatoInventario())
                        {
                            if (inventarios.Editar())
                            {
                                MessageBox.Show("Guardado Correctamente", "Correcto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Error al Actualizar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error al Actualizar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        CantidadtextBox.Text = inventarios.Existencia.ToString();
                        GuardarInventario();
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(CantidadtextBox.Text != "")
            {
                if (Convert.ToInt32(CantidadtextBox.Text) > 0)
                {
                    GuardarInventario();
                    this.Close();
                }
                
            }
            else
            {
                MessageBox.Show("Digite la Cantidad!");
            }

        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CantidadtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
