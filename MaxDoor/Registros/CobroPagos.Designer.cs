﻿namespace MaxDoor.Registros
{
    partial class CobroPagos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CobroPagos));
            this.label5 = new System.Windows.Forms.Label();
            this.ClientetextBox = new System.Windows.Forms.TextBox();
            this.CuotasdataGridView = new System.Windows.Forms.DataGridView();
            this.Exactobutton = new System.Windows.Forms.Button();
            this.ValorRecibidotextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.AbonotextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Cambiolabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ComentariotextBox = new System.Windows.Forms.TextBox();
            this.Cancelarbutton = new System.Windows.Forms.Button();
            this.Guardarbutton = new System.Windows.Forms.Button();
            this.Restantelabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Totallabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.CuotasdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label5.Location = new System.Drawing.Point(12, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 22);
            this.label5.TabIndex = 36;
            this.label5.Text = "Cliente";
            // 
            // ClientetextBox
            // 
            this.ClientetextBox.Enabled = false;
            this.ClientetextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientetextBox.Location = new System.Drawing.Point(92, 81);
            this.ClientetextBox.Name = "ClientetextBox";
            this.ClientetextBox.Size = new System.Drawing.Size(400, 31);
            this.ClientetextBox.TabIndex = 35;
            // 
            // CuotasdataGridView
            // 
            this.CuotasdataGridView.AllowUserToAddRows = false;
            this.CuotasdataGridView.AllowUserToDeleteRows = false;
            this.CuotasdataGridView.AllowUserToOrderColumns = true;
            this.CuotasdataGridView.AllowUserToResizeColumns = false;
            this.CuotasdataGridView.AllowUserToResizeRows = false;
            this.CuotasdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.CuotasdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CuotasdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.CuotasdataGridView.Location = new System.Drawing.Point(12, 118);
            this.CuotasdataGridView.Name = "CuotasdataGridView";
            this.CuotasdataGridView.RowHeadersVisible = false;
            this.CuotasdataGridView.Size = new System.Drawing.Size(748, 350);
            this.CuotasdataGridView.TabIndex = 37;
            // 
            // Exactobutton
            // 
            this.Exactobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Exactobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exactobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exactobutton.ForeColor = System.Drawing.Color.White;
            this.Exactobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exactobutton.Location = new System.Drawing.Point(175, 599);
            this.Exactobutton.Name = "Exactobutton";
            this.Exactobutton.Size = new System.Drawing.Size(159, 37);
            this.Exactobutton.TabIndex = 55;
            this.Exactobutton.Text = "Exacto";
            this.Exactobutton.UseVisualStyleBackColor = false;
            this.Exactobutton.Click += new System.EventHandler(this.Exactobutton_Click);
            // 
            // ValorRecibidotextBox
            // 
            this.ValorRecibidotextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValorRecibidotextBox.Location = new System.Drawing.Point(175, 563);
            this.ValorRecibidotextBox.Name = "ValorRecibidotextBox";
            this.ValorRecibidotextBox.Size = new System.Drawing.Size(159, 31);
            this.ValorRecibidotextBox.TabIndex = 52;
            this.ValorRecibidotextBox.TextChanged += new System.EventHandler(this.ValorRecibidotextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label6.Location = new System.Drawing.Point(185, 538);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 22);
            this.label6.TabIndex = 54;
            this.label6.Text = "Valor Recibido:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(183, 643);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 23);
            this.label7.TabIndex = 53;
            this.label7.Text = "Cambio:";
            // 
            // AbonotextBox
            // 
            this.AbonotextBox.Location = new System.Drawing.Point(175, 497);
            this.AbonotextBox.Name = "AbonotextBox";
            this.AbonotextBox.Size = new System.Drawing.Size(162, 27);
            this.AbonotextBox.TabIndex = 51;
            this.AbonotextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AbonotextBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 473);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 21);
            this.label1.TabIndex = 50;
            this.label1.Text = "Monto a Abonar";
            // 
            // Cambiolabel
            // 
            this.Cambiolabel.AutoSize = true;
            this.Cambiolabel.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cambiolabel.ForeColor = System.Drawing.Color.Red;
            this.Cambiolabel.Location = new System.Drawing.Point(270, 643);
            this.Cambiolabel.Name = "Cambiolabel";
            this.Cambiolabel.Size = new System.Drawing.Size(48, 23);
            this.Cambiolabel.TabIndex = 56;
            this.Cambiolabel.Text = "0.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label9.Location = new System.Drawing.Point(361, 473);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 21);
            this.label9.TabIndex = 64;
            this.label9.Text = "Comentario:";
            // 
            // ComentariotextBox
            // 
            this.ComentariotextBox.Location = new System.Drawing.Point(361, 497);
            this.ComentariotextBox.MaxLength = 150;
            this.ComentariotextBox.Multiline = true;
            this.ComentariotextBox.Name = "ComentariotextBox";
            this.ComentariotextBox.Size = new System.Drawing.Size(277, 83);
            this.ComentariotextBox.TabIndex = 63;
            // 
            // Cancelarbutton
            // 
            this.Cancelarbutton.BackColor = System.Drawing.SystemColors.Control;
            this.Cancelarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cancelarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelarbutton.Location = new System.Drawing.Point(613, 621);
            this.Cancelarbutton.Name = "Cancelarbutton";
            this.Cancelarbutton.Size = new System.Drawing.Size(147, 50);
            this.Cancelarbutton.TabIndex = 66;
            this.Cancelarbutton.Text = "Cancelar";
            this.Cancelarbutton.UseVisualStyleBackColor = false;
            // 
            // Guardarbutton
            // 
            this.Guardarbutton.BackColor = System.Drawing.Color.Gray;
            this.Guardarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardarbutton.ForeColor = System.Drawing.Color.White;
            this.Guardarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Guardarbutton.Image")));
            this.Guardarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Guardarbutton.Location = new System.Drawing.Point(460, 621);
            this.Guardarbutton.Name = "Guardarbutton";
            this.Guardarbutton.Size = new System.Drawing.Size(147, 50);
            this.Guardarbutton.TabIndex = 65;
            this.Guardarbutton.Text = "Guardar";
            this.Guardarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Guardarbutton.UseVisualStyleBackColor = false;
            this.Guardarbutton.Click += new System.EventHandler(this.Guardarbutton_Click);
            // 
            // Restantelabel
            // 
            this.Restantelabel.AutoSize = true;
            this.Restantelabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Restantelabel.ForeColor = System.Drawing.Color.Red;
            this.Restantelabel.Location = new System.Drawing.Point(183, 41);
            this.Restantelabel.Name = "Restantelabel";
            this.Restantelabel.Size = new System.Drawing.Size(68, 32);
            this.Restantelabel.TabIndex = 70;
            this.Restantelabel.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(10, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 32);
            this.label4.TabIndex = 69;
            this.label4.Text = "Restante:";
            // 
            // Totallabel
            // 
            this.Totallabel.AutoSize = true;
            this.Totallabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Totallabel.ForeColor = System.Drawing.Color.Black;
            this.Totallabel.Location = new System.Drawing.Point(183, 9);
            this.Totallabel.Name = "Totallabel";
            this.Totallabel.Size = new System.Drawing.Size(68, 32);
            this.Totallabel.TabIndex = 68;
            this.Totallabel.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(10, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 32);
            this.label8.TabIndex = 67;
            this.label8.Text = "Total:";
            // 
            // CobroPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(772, 683);
            this.Controls.Add(this.Restantelabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Totallabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Cancelarbutton);
            this.Controls.Add(this.Guardarbutton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ComentariotextBox);
            this.Controls.Add(this.Cambiolabel);
            this.Controls.Add(this.Exactobutton);
            this.Controls.Add(this.ValorRecibidotextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AbonotextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CuotasdataGridView);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ClientetextBox);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "CobroPagos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CobroPagos";
            this.Load += new System.EventHandler(this.CobroPagos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CuotasdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ClientetextBox;
        private System.Windows.Forms.DataGridView CuotasdataGridView;
        private System.Windows.Forms.Button Exactobutton;
        private System.Windows.Forms.TextBox ValorRecibidotextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox AbonotextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Cambiolabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ComentariotextBox;
        private System.Windows.Forms.Button Cancelarbutton;
        private System.Windows.Forms.Button Guardarbutton;
        private System.Windows.Forms.Label Restantelabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Totallabel;
        private System.Windows.Forms.Label label8;
    }
}