﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;
using PdfSharp;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace MaxDoor.Registros
{
    public partial class CobroPagos : Form
    {
        public static int IdVenta;
        public static double Total;
        public static double Restante;
        public static string NombreCliente;
        bool ExactoPress;
        Pagos pagos;
        DataTable DetallePagos = new DataTable();

        public CobroPagos()
        {
            InitializeComponent();
            pagos = new Pagos();
        }

        private void CobroPagos_Load(object sender, EventArgs e)
        {
            Cuotas cuotas = new Cuotas();
            CuotasdataGridView.DataSource = cuotas.Listado("NumeroCuota, FechaCuota as 'Fecha a Pagar', Monto, Balance", " Activo = 1 and IdVenta = " + IdVenta, "");
            CuotasdataGridView.Columns[2].DefaultCellStyle.Format = "N2";
            CuotasdataGridView.Columns[3].DefaultCellStyle.Format = "N2";

            Totallabel.Text = Total.ToString("N2");
            Restantelabel.Text = Restante.ToString("N2");
            ClientetextBox.Text = NombreCliente;
        }

        private void Exactobutton_Click(object sender, EventArgs e)
        {
            ExactoPress = true;
            if (Total > 0)
            {
                ValorRecibidotextBox.Text = Convert.ToDouble(AbonotextBox.Text).ToString("N2");
                Cambiolabel.Text = "0.00";
                ExactoPress = false;
            }
        }

        private void ValorRecibidotextBox_TextChanged(object sender, EventArgs e)
        {
            if (AbonotextBox.Text.Length > 0 && Convert.ToDouble(AbonotextBox.Text) > 0 && ValorRecibidotextBox.Text.Length > 0)
            {
                if (ExactoPress != true)
                {
                    Cambiolabel.Text = (Convert.ToInt32(ValorRecibidotextBox.Text) - Convert.ToDouble(AbonotextBox.Text)).ToString();
                }
            }
            else
            {
                Cambiolabel.Text = "0.00";
            }
        }

        private bool LlenarDatos()
        {
            bool retorno = true;

            pagos.IdVenta = IdVenta;
            pagos.MontoTotal = Convert.ToDouble(AbonotextBox.Text);
            pagos.Comentario = ComentariotextBox.Text;
            pagos.IdUsuario = LogIn.IdUsuario;

            return retorno;
        }

        private bool LlenarDatosDetalle(int NumeroCuota, double Monto)
        {
            bool retorno = true;

            pagos.IdPago = pagos.MaximoId();
            pagos.NumeroCuota = NumeroCuota;
            pagos.MontoDetalle = Monto;

            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(AbonotextBox.Text != "")
            {
                double MontoPago = Convert.ToDouble(AbonotextBox.Text);
                Cuotas cuotas = new Cuotas();
                Ventas ventas = new Ventas();
                DetallePagos.Columns.Add("Cuota #");
                DetallePagos.Columns.Add("Monto");

                if (MontoPago <= Restante)
                {
                    if (LlenarDatos())
                    {
                        if (pagos.Insertar())
                        {
                            do
                            {
                                if (cuotas.BuscarProxCuota(IdVenta))
                                {
                                    if (MontoPago > cuotas.Balance)
                                    {
                                        if (cuotas.CompletarCuota(cuotas.Balance, cuotas.IdCuota))
                                        {
                                            if (pagos.ActualizarBalance(IdVenta, cuotas.Balance))
                                            {
                                                if (LlenarDatosDetalle(cuotas.NumeroCuota, cuotas.Balance))
                                                {
                                                    if (pagos.InsertarDetalle())
                                                    {
                                                        DetallePagos.Rows.Add(cuotas.NumeroCuota, cuotas.Balance);
                                                        MontoPago -= cuotas.Balance;
                                                    }
                                                }


                                            }
                                        }
                                    }
                                    else if (MontoPago < cuotas.Balance)
                                    {
                                        if (cuotas.ActualizarBalance(MontoPago, cuotas.IdCuota))
                                        {
                                            if (pagos.ActualizarBalance(IdVenta, MontoPago))
                                            {

                                                if (LlenarDatosDetalle(cuotas.NumeroCuota, MontoPago))
                                                {
                                                    if (pagos.InsertarDetalle())
                                                    {
                                                        DetallePagos.Rows.Add(cuotas.NumeroCuota, MontoPago);
                                                        MontoPago = 0;
                                                        if (!cuotas.BuscarProxCuota(IdVenta))
                                                        {
                                                            ventas.CompletarVenta(IdVenta);
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                    }
                                    else if (MontoPago == cuotas.Balance)
                                    {
                                        if (cuotas.CompletarCuota(MontoPago, cuotas.IdCuota))
                                        {
                                            if (pagos.ActualizarBalance(IdVenta, MontoPago))
                                            {
                                                if (LlenarDatosDetalle(cuotas.NumeroCuota, MontoPago))
                                                {
                                                    if (pagos.InsertarDetalle())
                                                    {
                                                        DetallePagos.Rows.Add(cuotas.NumeroCuota, MontoPago);
                                                        MontoPago = 0;
                                                        if (!cuotas.BuscarProxCuota(IdVenta))
                                                        {
                                                            ventas.CompletarVenta(IdVenta);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }

                            } while (MontoPago > 0);

                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                            CrearPDF();
                        }
                    }


                }
                else
                {
                    MessageBox.Show("El Monto digitado es mayor al Balance de la cuenta!");
                }
            }
            else
            {
                MessageBox.Show("Digite el monto a Abonar!");
            }
            
            
        }

        public void CrearPDF()
        {
            string TablePagos = "";
            for (int i = 0; i < DetallePagos.Rows.Count; i++)
            {
                TablePagos += "<tr> <td class=\"td1\" > " + DetallePagos.Rows[i][0].ToString() + " </td> <td class=\"td1\">" + Convert.ToDouble(DetallePagos.Rows[i][1]).ToString("N2") + "</td> </tr>";
            }

            string Comentario = "";
            if (ComentariotextBox.Text.Length > 0)
            {
                Comentario = "Comentario: " + ComentariotextBox.Text;
            }
            string html = "<style>.container { width:100%; font-family: arial, sans-serif; }table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; } th { background-color: darkgrey; border-radius: 5px; }td,th { border: 1px solid #dddddd; } .container2 { width:100%; height: 200px; }.div1 { float:left; margin-left: 15px; width:45%; }.div2 { text-align:center;  margin-right: auto; margin-left: auto; width:100%; }.div3 { width:100%; text-align: center; } .div4 { text-align:left; margin-left: 15px; margin-bottom: 35px; } .w18 { width:18%; } .w46 { width:46%; } .td1 { text-align:center; } .td3-4 { text-align:right; } .last-div { text-align: right; margin-right: 25px; } </style><div class=\"container\"> <div class=\"div2\" > <strong>Max Door<br>Calle Principal<br>809-246-2567<br>RNC: 16324656</strong></div> <div class=\"container2\" > <div class=\"div1\" ><img src=\"maxdoorlogo.png\" width =\"200px\" alt =\"\" ></div> </div>  <div class=\"div3\" ><h2>***COMPROBANTE DE PAGO***</h2></div> <div style=\"text-align:left; margin-left: 15px; margin-bottom: 35px; \"><strong>No: 00128<br> " + DateTime.Now.ToString("dd/MM/yyyy") + " <br>Hora: " + DateTime.Now.ToString("hh:mm:ss") + " <br>Cajero: "+ LogIn.NombreUsuario +" <br>Cliente: "+ NombreCliente +" <br>No. de Venta: "+ IdVenta + "</strong></div> <table> <tr> <th class=\"w18\" > Cuota #</th> <th class=\"w46\" > Monto</th></tr>" + TablePagos +" </table> <div style=\"text-align: right; margin-right: 25px; \"><h3>TOTAL ABONADO: "+ Convert.ToDouble(AbonotextBox.Text).ToString("N2") +"</h3></div> <div style=\"text-align: right; margin-right: 25px; \"><h3>Resta: "+ (Convert.ToDouble(Restantelabel.Text) - Convert.ToDouble(AbonotextBox.Text)).ToString("N2") + "</h3></div> <div style=\"margin-top:25px\">" + Comentario + "</div></div>";

            string path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\MaxDoor - Facturas y Contratos\\Pagos";
            Directory.CreateDirectory(path);
            PdfDocument pdf = PdfGenerator.GeneratePdf(html, PageSize.Letter);
            string DocName;
            DocName = "Pago " + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " + DateTime.Now.ToString("hh.mm.ss") + ".pdf";
            pdf.Save(path + "\\" + DocName);
            Process.Start(path + "\\" + DocName);
        }

        private void AbonotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
