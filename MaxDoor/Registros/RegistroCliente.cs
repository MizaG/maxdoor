﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroCliente : Form
    {
        private Clientes cliente;
        public static int IdEdicion;

        public RegistroCliente()
        {
            InitializeComponent();
            cliente = new Clientes();
        }

        private bool LlenarDatos()
        {
            bool retorno = true;

            cliente.Cedula = CedulamaskedTextBox.Text;
            cliente.Nombres = NombrestextBox.Text;
            cliente.Apellidos = ApellidostextBox.Text;
            cliente.Direccion = DirecciontextBox.Text;
            cliente.Telefono = TelefonomaskedTextBox.Text;

            return retorno;
        }

        public void LlenarCampos()
        {
            if (cliente.Buscar(IdEdicion))
            {
                CedulamaskedTextBox.Text = cliente.Cedula;
                NombrestextBox.Text = cliente.Nombres;
                ApellidostextBox.Text = cliente.Apellidos;
                DirecciontextBox.Text = cliente.Direccion;
                TelefonomaskedTextBox.Text = cliente.Telefono;
            }
            
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(NombrestextBox.Text != "")
            {
                if (LlenarDatos())
                {
                    if (IdEdicion > 0)
                    {
                        if (cliente.Editar(IdEdicion))
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        if (cliente.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Digite los datos del cliente!");
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
            IdEdicion = 0;
        }

        private void RegistroCliente_Load(object sender, EventArgs e)
        {
            if(IdEdicion > 0)
            {
                LlenarCampos();
            }
        }
    }
}
