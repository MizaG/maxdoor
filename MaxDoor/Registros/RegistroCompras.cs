﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroCompras : Form
    {
        public static string[] InfoProducto = new string[3];
        public double Total;
        Compras compras;
        public RegistroCompras()
        {
            InitializeComponent();
            compras = new Compras();
        }

        public void RetornarProducto()
        {
            NombreProductotextBox.Tag = InfoProducto[0];
            NombreProductotextBox.Text = InfoProducto[1];
            PreciotextBox.Text = InfoProducto[2];
        }

        private void BuscarProductobutton_Click(object sender, EventArgs e)
        {
            BusquedaGeneral.Identificador = 3;
            BusquedaGeneral busqueda = new BusquedaGeneral();
            busqueda.ShowDialog();
            RetornarProducto();
            CantidadtextBox.Focus();
        }

        private void Agregarbutton_Click(object sender, EventArgs e)
        {
            AgregarProducto();
        }

        public void AgregarProducto()
        {
            if(CantidadtextBox.Text != "")
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(ProductosdataGridView);
                fila.Cells[0].Value = NombreProductotextBox.Tag;
                fila.Cells[1].Value = NombreProductotextBox.Text;
                fila.Cells[2].Value = CantidadtextBox.Text;
                fila.Cells[3].Value = PreciotextBox.Text;
                fila.Cells[4].Value = (Convert.ToDouble(PreciotextBox.Text) * Convert.ToDouble(CantidadtextBox.Text)).ToString();
                ProductosdataGridView.Rows.Add(fila);
                ActualizarTotal();
                LimpiarProducto();
            }
            else
            {
                MessageBox.Show("Digite la cantidad!");
            }
            
        }

        public void LimpiarProducto()
        {
            NombreProductotextBox.Tag = "";
            NombreProductotextBox.Text = "";
            PreciotextBox.Text = "";
            CantidadtextBox.Text = "";
        }

        public void ActualizarTotal()
        {
            Total = 0;

            foreach (DataGridViewRow row in ProductosdataGridView.Rows)
            {
                Total += Convert.ToDouble(row.Cells[4].Value);
            }

            Totallabel.Text = Total.ToString("N2");
        }

        private void Eliminarbutton_Click(object sender, EventArgs e)
        {
            DialogResult result;

            result = MessageBox.Show("Desea eliminar el articulo seleccionado?", "", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                ProductosdataGridView.Rows.RemoveAt(ProductosdataGridView.CurrentRow.Index);
            }
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool LlenarDatosCompra()
        {
            bool retorno = true;

            compras.IdProveedor = Convert.ToInt32(ProveedorcomboBox.SelectedValue);
            compras.IdUsuario = LogIn.IdUsuario;
            compras.NCF = NCFtextBox.Text;
            compras.MontoTotal = Convert.ToDouble(Totallabel.Text);

            compras.LimpiarList();

            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                compras.AgregarProductos(Convert.ToInt32(ProductosdataGridView.Rows[i].Cells[0].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[2].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[3].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[4].Value));

            }
            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(ProductosdataGridView.Rows.Count > 0)
            {
                if (LlenarDatosCompra())
                {
                    if (compras.Insertar())
                    {
                        MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                        //Imprimir Factura de Compra

                        for (int i = 0; i < ProductosdataGridView.RowCount; i++)
                        {
                            Inventarios inventarios = new Inventarios();
                            inventarios.ActualizarInventario(Convert.ToInt32(ProductosdataGridView.Rows[i].Cells[0].Value), "+", Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[2].Value));
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar al menos 1 articulo");
            }
            
        }

        private void RegistroCompras_Load(object sender, EventArgs e)
        {
            Proveedores proveedores = new Proveedores();
            ProveedorcomboBox.DataSource = proveedores.Listado("Empresa, IdProveedor", " Activo = 1 ", "");
            ProveedorcomboBox.DisplayMember = "Empresa";
            ProveedorcomboBox.ValueMember = "IdProveedor";
        }

        private void CantidadtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == Convert.ToChar(Keys.Enter))
            {
                AgregarProducto();
            }
        }

        private void CantidadtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
