﻿namespace MaxDoor.Registros
{
    partial class RegistroInventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ProductosdataGridView = new System.Windows.Forms.DataGridView();
            this.ExistenciadataGridView = new System.Windows.Forms.DataGridView();
            this.Restarbutton = new System.Windows.Forms.Button();
            this.Agregarbutton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Okbutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistenciadataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inventario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(220, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 22);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lista de Materiales y Productos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(872, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Existencia";
            // 
            // ProductosdataGridView
            // 
            this.ProductosdataGridView.AllowUserToAddRows = false;
            this.ProductosdataGridView.AllowUserToDeleteRows = false;
            this.ProductosdataGridView.AllowUserToOrderColumns = true;
            this.ProductosdataGridView.AllowUserToResizeColumns = false;
            this.ProductosdataGridView.AllowUserToResizeRows = false;
            this.ProductosdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ProductosdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductosdataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProductosdataGridView.Location = new System.Drawing.Point(12, 128);
            this.ProductosdataGridView.Name = "ProductosdataGridView";
            this.ProductosdataGridView.RowHeadersVisible = false;
            this.ProductosdataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProductosdataGridView.Size = new System.Drawing.Size(706, 360);
            this.ProductosdataGridView.TabIndex = 3;
            // 
            // ExistenciadataGridView
            // 
            this.ExistenciadataGridView.AllowUserToAddRows = false;
            this.ExistenciadataGridView.AllowUserToDeleteRows = false;
            this.ExistenciadataGridView.AllowUserToResizeColumns = false;
            this.ExistenciadataGridView.AllowUserToResizeRows = false;
            this.ExistenciadataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ExistenciadataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ExistenciadataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ExistenciadataGridView.Location = new System.Drawing.Point(724, 128);
            this.ExistenciadataGridView.Name = "ExistenciadataGridView";
            this.ExistenciadataGridView.RowHeadersVisible = false;
            this.ExistenciadataGridView.Size = new System.Drawing.Size(394, 360);
            this.ExistenciadataGridView.TabIndex = 4;
            // 
            // Restarbutton
            // 
            this.Restarbutton.BackColor = System.Drawing.Color.Red;
            this.Restarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Restarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Restarbutton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Restarbutton.Location = new System.Drawing.Point(480, 516);
            this.Restarbutton.Name = "Restarbutton";
            this.Restarbutton.Size = new System.Drawing.Size(78, 58);
            this.Restarbutton.TabIndex = 44;
            this.Restarbutton.Text = "-";
            this.Restarbutton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Restarbutton.UseVisualStyleBackColor = false;
            this.Restarbutton.Click += new System.EventHandler(this.Restarbutton_Click);
            // 
            // Agregarbutton
            // 
            this.Agregarbutton.BackColor = System.Drawing.Color.Green;
            this.Agregarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Agregarbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Agregarbutton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Agregarbutton.Location = new System.Drawing.Point(323, 516);
            this.Agregarbutton.Name = "Agregarbutton";
            this.Agregarbutton.Size = new System.Drawing.Size(78, 58);
            this.Agregarbutton.TabIndex = 43;
            this.Agregarbutton.Text = "+\r\n";
            this.Agregarbutton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Agregarbutton.UseVisualStyleBackColor = false;
            this.Agregarbutton.Click += new System.EventHandler(this.Agregarbutton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(320, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 22);
            this.label4.TabIndex = 45;
            this.label4.Text = "Entrada";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(487, 491);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 22);
            this.label5.TabIndex = 46;
            this.label5.Text = "Salida";
            // 
            // Okbutton
            // 
            this.Okbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Okbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Okbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Okbutton.ForeColor = System.Drawing.Color.White;
            this.Okbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Okbutton.Location = new System.Drawing.Point(1023, 520);
            this.Okbutton.Name = "Okbutton";
            this.Okbutton.Size = new System.Drawing.Size(95, 50);
            this.Okbutton.TabIndex = 47;
            this.Okbutton.Text = "O.K.";
            this.Okbutton.UseVisualStyleBackColor = false;
            this.Okbutton.Click += new System.EventHandler(this.Okbutton_Click);
            // 
            // RegistroInventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1129, 582);
            this.Controls.Add(this.Okbutton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Restarbutton);
            this.Controls.Add(this.Agregarbutton);
            this.Controls.Add(this.ExistenciadataGridView);
            this.Controls.Add(this.ProductosdataGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "RegistroInventario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RegistroInventario";
            this.Load += new System.EventHandler(this.RegistroInventario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProductosdataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistenciadataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView ProductosdataGridView;
        private System.Windows.Forms.DataGridView ExistenciadataGridView;
        private System.Windows.Forms.Button Restarbutton;
        private System.Windows.Forms.Button Agregarbutton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Okbutton;
    }
}