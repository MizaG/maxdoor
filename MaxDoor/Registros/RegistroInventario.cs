﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroInventario : Form
    {
        Productos productos;
        Inventarios inventarios;
        public RegistroInventario()
        {
            InitializeComponent();
            productos = new Productos();
            inventarios = new Inventarios();
        }

        private void CargarInventario()
        {
            ExistenciadataGridView.DataSource = inventarios.Listado("p.Nombre, i.Existencia, p.UnidadMedida as 'Medida'", " i.IdProducto = p.IdProducto", "");
            ExistenciadataGridView.Columns[0].Width = 220;
        }

        private void Agregarbutton_Click(object sender, EventArgs e)
        {
            CantidadInventario.IdProducto = Convert.ToInt32(ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[0].Value);
            CantidadInventario.Sumar = true;
            CantidadInventario cantidad = new CantidadInventario();
            cantidad.NombretextBox.Text = ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[1].Value.ToString();
            cantidad.ShowDialog();
            CargarInventario();
        }

        private void RegistroInventario_Load(object sender, EventArgs e)
        {
            ProductosdataGridView.DataSource = productos.ListadoInventario();
            ProductosdataGridView.Columns[0].Visible = false;
            CargarInventario();
        }

        private void Restarbutton_Click(object sender, EventArgs e)
        {
            CantidadInventario.IdProducto = Convert.ToInt32(ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[0].Value);
            CantidadInventario.Sumar = false;
            CantidadInventario cantidad = new CantidadInventario();
            cantidad.NombretextBox.Text = ProductosdataGridView.Rows[ProductosdataGridView.CurrentRow.Index].Cells[1].Value.ToString();
            cantidad.ShowDialog();
            CargarInventario();
        }

        private void Okbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
