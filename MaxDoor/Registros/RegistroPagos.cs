﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroPagos : Form
    {
        public RegistroPagos()
        {
            InitializeComponent();
            FiltrotextBox.Focus();
        }

        private void RegistroPagos_Load(object sender, EventArgs e)
        {
            Ventas ventas = new Ventas();

            VentasdataGridView.DataSource = ventas.ListadoParaPagos();
            VentasdataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            VentasdataGridView.Columns[0].Width = 60;
            VentasdataGridView.Columns[1].Width = 130;
            VentasdataGridView.Columns[3].Width = 130;
            VentasdataGridView.Columns[4].Width = 160;

            VentasdataGridView.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            VentasdataGridView.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            VentasdataGridView.Columns[3].DefaultCellStyle.Format = "N2";
            VentasdataGridView.Columns[4].DefaultCellStyle.Format = "N2";

            
        }

        private void FechacheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FechacheckBox.Checked)
            {
                DesdedateTimePicker.Enabled = true;
                HastadateTimePicker.Enabled = true;
                Buscarbutton.Enabled = true;
            }
            else if (!FechacheckBox.Checked)
            {
                DesdedateTimePicker.Enabled = false;
                HastadateTimePicker.Enabled = false;
                Buscarbutton.Enabled = false;
            }
        }

        private void FiltrotextBox_TextChanged(object sender, EventArgs e)
        {
            
            if (FechacheckBox.Checked)
            {
                BuscarXFecha();
            }
            else
            {
                Ventas ventas = new Ventas();
                VentasdataGridView.DataSource = ventas.ListadoParaPagosFiltro(FiltrotextBox.Text);
            }
            
        }

        public void BuscarXFecha()
        {
            Ventas ventas = new Ventas();
            VentasdataGridView.DataSource = ventas.ListadoParaPagosFiltroFecha(FiltrotextBox.Text, DesdedateTimePicker.Value.Date.ToString("yyyy/MM/dd"), HastadateTimePicker.Value.Date.ToString("yyyy/MM/dd"));
        }

        private void Buscarbutton_Click(object sender, EventArgs e)
        {
            BuscarXFecha();
        }

        private void FiltrotextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == Convert.ToChar(Keys.Down))
            {
                VentasdataGridView.Focus();
            }
        }

        private void Abonarbutton_Click(object sender, EventArgs e)
        {
            CobroPagos.IdVenta = Convert.ToInt32(VentasdataGridView.CurrentRow.Cells[0].Value.ToString());
            CobroPagos.Total = Convert.ToDouble(VentasdataGridView.CurrentRow.Cells[3].Value.ToString());
            CobroPagos.Restante = Convert.ToDouble(VentasdataGridView.CurrentRow.Cells[4].Value.ToString());
            CobroPagos.NombreCliente = VentasdataGridView.CurrentRow.Cells[2].Value.ToString();
            CobroPagos cobroPagos = new CobroPagos();
            cobroPagos.ShowDialog();
            this.Close();
        }
    }
}
