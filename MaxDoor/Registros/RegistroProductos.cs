﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroProductos : Form
    {
        Productos productos;
        public static int IdEdicion;
        public RegistroProductos()
        {
            InitializeComponent();
            productos = new Productos();
        }

        private void RegistroProductos_Load(object sender, EventArgs e)
        {
            Productos productos = new Productos();
            TipoProductocomboBox.DataSource = productos.ListadoTipoProductos(" * ", " 1 = 1 ", "");
            TipoProductocomboBox.DisplayMember = "TipoProducto";
            TipoProductocomboBox.ValueMember = "IdTipoProducto";

            Proveedores proveedores = new Proveedores();
            ProveedorcomboBox.DataSource = proveedores.Listado("Empresa, IdProveedor", " Activo = 1 ", "");
            ProveedorcomboBox.DisplayMember = "Empresa";
            ProveedorcomboBox.ValueMember = "IdProveedor";

            if(IdEdicion > 0)
            {
                LlenarCampos();
            }
        }

        private void TipoProductocomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(TipoProductocomboBox.Text == "Material")
            {
                UnidadMedidacomboBox.Visible = true;
                UnidadMedidalabel.Visible = true;
            }
            else
            {
                UnidadMedidacomboBox.Visible = false;
                UnidadMedidalabel.Visible = false;
            }
        }

        public bool LlenarDatos()
        {
            bool retorno = true;

            productos.Nombre = NombretextBox.Text;
            productos.Precio = Convert.ToDouble(PreciotextBox.Text);
            productos.Costo = Convert.ToDouble(CostotextBox.Text);
            productos.IdTipoProducto = Convert.ToInt32(TipoProductocomboBox.SelectedValue);
            productos.IdProveedor = Convert.ToInt32(ProveedorcomboBox.SelectedValue);
            productos.UnidadMedida = UnidadMedidacomboBox.Text;
            productos.PrecioMinimo = Convert.ToDouble(PrecioMinimotextBox.Text);

            return retorno;
        }

        public void LlenarCampos()
        {
            if (productos.Buscar(IdEdicion))
            {
                NombretextBox.Text = productos.Nombre;
                PreciotextBox.Text = productos.Precio.ToString();
                CostotextBox.Text = productos.Costo.ToString();
                TipoProductocomboBox.SelectedValue = productos.IdTipoProducto;
                ProveedorcomboBox.SelectedValue = productos.IdProveedor;
                UnidadMedidacomboBox.Text = productos.UnidadMedida;
                PrecioMinimotextBox.Text = productos.PrecioMinimo.ToString();
            }
            
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(NombretextBox.Text != "" && PreciotextBox.Text != "" && CostotextBox.Text != "")
            {
                if (LlenarDatos())
                {
                    if (IdEdicion > 0)
                    {
                        if (productos.Editar(IdEdicion))
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        if (productos.Insertar())
                        {
                            Inventarios inventarios = new Inventarios();
                            inventarios.IdProducto = productos.MaximoId();
                            inventarios.Existencia = 0;
                            if (inventarios.Insertar())
                            {
                                MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                this.Close();
                            }

                        }
                    }

                }
            }
            else
            {
                MessageBox.Show("Digite los datos del articulo!");
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PreciotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void CostotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
