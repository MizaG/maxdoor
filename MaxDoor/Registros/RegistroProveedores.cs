﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroProveedores : Form
    {
        private Proveedores proveedor;
        public static int IdEdicion;
        public RegistroProveedores()
        {
            InitializeComponent();
            proveedor = new Proveedores();
        }

        public bool LlenarClase()
        {
            bool retorno = true;

            proveedor.Empresa = EmpresatextBox.Text;
            proveedor.Representante = RepresentantetextBox.Text;
            proveedor.RNC = RNCtextBox.Text;
            proveedor.Direccion = DirecciontextBox.Text;
            proveedor.Telefono = TelefonomaskedTextBox.Text;
            proveedor.Celular = CelularmaskedTextBox.Text;
            proveedor.Correo = CorreotextBox.Text;

            return retorno;
        }

        public void LlenarCampos()
        {
            if (proveedor.Buscar(IdEdicion))
            {
                EmpresatextBox.Text = proveedor.Empresa;
                RepresentantetextBox.Text = proveedor.Representante;
                RNCtextBox.Text = proveedor.RNC;
                DirecciontextBox.Text = proveedor.Direccion;
                TelefonomaskedTextBox.Text = proveedor.Telefono;
                CelularmaskedTextBox.Text = proveedor.Celular;
                CorreotextBox.Text = proveedor.Correo;
            }
            
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(EmpresatextBox.Text != "")
            {
                if (LlenarClase())
                {
                    if (IdEdicion > 0)
                    {
                        if (proveedor.Editar(IdEdicion))
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        if (proveedor.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }

                }
            }
            else
            {
                MessageBox.Show("Digite los datos del Proveedor!");
            }

        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RegistroProveedores_Load(object sender, EventArgs e)
        {
            if(IdEdicion > 0)
            {
                LlenarCampos();
            }
        }
    }
}
