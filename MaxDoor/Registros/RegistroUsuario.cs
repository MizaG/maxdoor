﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroUsuario : Form
    {
        Usuarios usuarios;
        public static int IdEdicion;

        public RegistroUsuario()
        {
            InitializeComponent();
            usuarios = new Usuarios();
        }

        private bool LlenarDatos()
        {
            bool retorno = true;

            usuarios.Nombres = NombrestextBox.Text;
            usuarios.Apellidos = ApellidostextBox.Text;
            usuarios.NombreUsuario = UsuariotextBox.Text;
            usuarios.Pass = PasstextBox.Text;
            usuarios.Rol = RolcomboBox.SelectedIndex;
            usuarios.FechaCreacion = DateTime.Now.ToString();

            return retorno;
        }

        public void LlenarCampos()
        {
            if (usuarios.Buscar(IdEdicion))
            {
                NombrestextBox.Text = usuarios.Nombres;
                ApellidostextBox.Text = usuarios.Apellidos;
                UsuariotextBox.Text = usuarios.NombreUsuario;
                RolcomboBox.SelectedIndex = usuarios.Rol;
            }
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(UsuariotextBox.Text != "" && PasstextBox.Text != "" && NombrestextBox.Text != "" && ApellidostextBox.Text != "" && RolcomboBox.Text != "")
            {
                if (LlenarDatos())
                {
                    if (IdEdicion > 0)
                    {
                        if (usuarios.Editar(IdEdicion))
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        if (usuarios.Insertar())
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Digite los datos del Usuario!");
            }
            
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RegistroUsuario_Load(object sender, EventArgs e)
        {
            if(IdEdicion > 0)
            {
                LlenarCampos();
            }
        }
    }
}
