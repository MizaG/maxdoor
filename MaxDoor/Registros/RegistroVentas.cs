﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;

namespace MaxDoor.Registros
{
    public partial class RegistroVentas : Form
    {
        public static int IdCliente;
        public static string NombreCliente;
        public static string NombreProducto;
        public static string[] InfoProducto = new string[3];
        public double Total;
        private Ventas ventas;

        public RegistroVentas()
        {
            InitializeComponent();
            CargarTipoVentas();
            ventas = new Ventas();
        }

        public void CargarTipoVentas()
        {
            Ventas ventas = new Ventas();
            TipoVentacomboBox.DataSource = ventas.ListadoTipoVentas(" * ", " 1=1 ", "");
            TipoVentacomboBox.DisplayMember = "TipoVenta";
            TipoVentacomboBox.ValueMember = "IdTipoVenta";
        }

        public void RetornarCliente()
        {
            ClientetextBox.Tag = IdCliente.ToString();
            ClientetextBox.Text = NombreCliente;
        }

        public void RetornarProducto()
        {
            NombreProductotextBox.Tag = InfoProducto[0];
            NombreProductotextBox.Text = InfoProducto[1];
            PreciotextBox.Text = InfoProducto[2];
        }

        private void BuscarClientebutton_Click(object sender, EventArgs e)
        {
            BusquedaGeneral.Identificador = 1;
            BusquedaGeneral busqueda = new BusquedaGeneral();
            busqueda.ShowDialog();
            RetornarCliente();
        }

        private void Agregarbutton_Click(object sender, EventArgs e)
        {
            AgregarProducto();
        }

        private void BuscarProductobutton_Click(object sender, EventArgs e)
        {
            BusquedaGeneral.Identificador = 2;
            BusquedaGeneral busqueda = new BusquedaGeneral();
            busqueda.ShowDialog();
            RetornarProducto();
            CantidadtextBox.Focus();
        }

        public void AgregarProducto()
        {
            if(CantidadtextBox.Text != "")
            {
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(ProductosdataGridView);
                fila.Cells[0].Value = NombreProductotextBox.Tag;
                fila.Cells[1].Value = NombreProductotextBox.Text;
                fila.Cells[2].Value = CantidadtextBox.Text;
                fila.Cells[3].Value = PreciotextBox.Text;
                fila.Cells[4].Value = (Convert.ToDouble(PreciotextBox.Text) * Convert.ToDouble(CantidadtextBox.Text)).ToString();
                ProductosdataGridView.Rows.Add(fila);
                ActualizarTotal();
                LimpiarProducto();
            }
            else
            {
                MessageBox.Show("Digite la cantidad!");
            }
        }

        public bool LlenarDatosVenta()
        {
            bool retorno = true;

            ventas.IdUsuario = LogIn.IdUsuario;
            ventas.IdCliente = Convert.ToInt32(ClientetextBox.Tag);
            ventas.IdTipoVenta = Convert.ToInt32(TipoVentacomboBox.SelectedValue);
            ventas.Completada = false;
            ventas.MontoTotal = Convert.ToDouble(Totallabel.Text);
            ventas.CantidadCuotas = 0;
            ventas.Balance = Convert.ToDouble(Totallabel.Text);

            ventas.LimpiarList();

            for (int i = 0; i < ProductosdataGridView.RowCount; i++)
            {
                ventas.AgregarProductos(Convert.ToInt32(ProductosdataGridView.Rows[i].Cells[0].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[2].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[3].Value), Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[4].Value));

            }
            return retorno;
        } 

        public void ActualizarTotal()
        {
            Total = 0;

            foreach (DataGridViewRow row in ProductosdataGridView.Rows)
            {
                Total += Convert.ToDouble(row.Cells[4].Value);
            }

            Totallabel.Text = Total.ToString("N2");
        }

        private void CantidadtextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == Convert.ToChar(Keys.Enter))
            {
                AgregarProducto();
            }
        }

        public void LimpiarProducto()
        {
            NombreProductotextBox.Tag = "";
            NombreProductotextBox.Text = "";
            PreciotextBox.Text = "";
            CantidadtextBox.Text = "";
        }

        private void Cancelarbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Eliminarbutton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result;
                result = MessageBox.Show("Desea eliminar el articulo seleccionado?", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    ProductosdataGridView.Rows.RemoveAt(ProductosdataGridView.CurrentRow.Index);
                    ActualizarTotal();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Debe Seleccionar el producto que desea Eliminar!");
            }
                

        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(ProductosdataGridView.Rows.Count > 0)
            {
                if (LlenarDatosVenta())
                {
                    if (ventas.Insertar())
                    {
                        if (Convert.ToInt32(TipoVentacomboBox.SelectedValue) == 1)
                        {
                            VentaContado ventaContado = new VentaContado();
                            VentaContado.IdCliente = Convert.ToInt32(ClientetextBox.Tag);
                            VentaContado.NombreCliente = ClientetextBox.Text;
                            VentaContado.Total = Convert.ToDouble(Totallabel.Text);
                            VentaContado.IdVenta = ventas.MaximoId();
                            ventaContado.ShowDialog();
                        }
                        else if (Convert.ToInt32(TipoVentacomboBox.SelectedValue) == 2)
                        {
                            VentaCredito ventaCredito = new VentaCredito();
                            VentaCredito.IdCliente = Convert.ToInt32(ClientetextBox.Tag);
                            VentaCredito.NombreCliente = ClientetextBox.Text;
                            VentaCredito.Total = Convert.ToDouble(Totallabel.Text);
                            VentaCredito.IdVenta = ventas.MaximoId();
                            ventaCredito.ShowDialog();
                        }

                        for (int i = 0; i < ProductosdataGridView.RowCount; i++)
                        {
                            Inventarios inventarios = new Inventarios();
                            inventarios.ActualizarInventario(Convert.ToInt32(ProductosdataGridView.Rows[i].Cells[0].Value), "-", Convert.ToDouble(ProductosdataGridView.Rows[i].Cells[2].Value));
                        }

                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar al menos 1 articulo!");
            }
            
        }

        private void CantidadtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void PrecioMinimobutton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result;
                result = MessageBox.Show("Desea aplicar precio minimo a este producto?", "", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    Productos productos = new Productos();
                    if (productos.Buscar(Convert.ToInt32(ProductosdataGridView.CurrentRow.Cells[0].Value)))
                    {
                        ProductosdataGridView.CurrentRow.Cells[3].Value = productos.PrecioMinimo.ToString();
                        ProductosdataGridView.CurrentRow.Cells[4].Value = (Convert.ToInt32(ProductosdataGridView.CurrentRow.Cells[2].Value) * Convert.ToInt32(ProductosdataGridView.CurrentRow.Cells[3].Value)).ToString();
                        ActualizarTotal();
                    }

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Debe Seleccionar un producto!");
            }
        }
    }
}
