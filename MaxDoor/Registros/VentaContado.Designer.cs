﻿namespace MaxDoor.Registros
{
    partial class VentaContado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentaContado));
            this.Exactobutton = new System.Windows.Forms.Button();
            this.ValorRecibidotextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Cambiolabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ClientetextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ComentariotextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Totallabel = new System.Windows.Forms.Label();
            this.Guardarbutton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Exactobutton
            // 
            this.Exactobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Exactobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exactobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exactobutton.ForeColor = System.Drawing.Color.White;
            this.Exactobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exactobutton.Location = new System.Drawing.Point(90, 195);
            this.Exactobutton.Name = "Exactobutton";
            this.Exactobutton.Size = new System.Drawing.Size(145, 37);
            this.Exactobutton.TabIndex = 32;
            this.Exactobutton.Text = "Exacto";
            this.Exactobutton.UseVisualStyleBackColor = false;
            this.Exactobutton.Click += new System.EventHandler(this.Exactobutton_Click);
            // 
            // ValorRecibidotextBox
            // 
            this.ValorRecibidotextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValorRecibidotextBox.Location = new System.Drawing.Point(90, 159);
            this.ValorRecibidotextBox.Name = "ValorRecibidotextBox";
            this.ValorRecibidotextBox.Size = new System.Drawing.Size(145, 31);
            this.ValorRecibidotextBox.TabIndex = 25;
            this.ValorRecibidotextBox.TextChanged += new System.EventHandler(this.ValorRecibidotextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label6.Location = new System.Drawing.Point(86, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 22);
            this.label6.TabIndex = 28;
            this.label6.Text = "Valor Recibido:";
            // 
            // Cambiolabel
            // 
            this.Cambiolabel.AutoSize = true;
            this.Cambiolabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cambiolabel.ForeColor = System.Drawing.Color.Red;
            this.Cambiolabel.Location = new System.Drawing.Point(176, 239);
            this.Cambiolabel.Name = "Cambiolabel";
            this.Cambiolabel.Size = new System.Drawing.Size(68, 32);
            this.Cambiolabel.TabIndex = 27;
            this.Cambiolabel.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(55, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 32);
            this.label4.TabIndex = 26;
            this.label4.Text = "Cambio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label5.Location = new System.Drawing.Point(10, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 22);
            this.label5.TabIndex = 34;
            this.label5.Text = "Cliente";
            // 
            // ClientetextBox
            // 
            this.ClientetextBox.Enabled = false;
            this.ClientetextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientetextBox.Location = new System.Drawing.Point(90, 88);
            this.ClientetextBox.Name = "ClientetextBox";
            this.ClientetextBox.Size = new System.Drawing.Size(455, 31);
            this.ClientetextBox.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label11.Location = new System.Drawing.Point(268, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 21);
            this.label11.TabIndex = 31;
            this.label11.Text = "Comentario:";
            // 
            // ComentariotextBox
            // 
            this.ComentariotextBox.Location = new System.Drawing.Point(268, 159);
            this.ComentariotextBox.MaxLength = 150;
            this.ComentariotextBox.Multiline = true;
            this.ComentariotextBox.Name = "ComentariotextBox";
            this.ComentariotextBox.Size = new System.Drawing.Size(277, 83);
            this.ComentariotextBox.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(189, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 32);
            this.label8.TabIndex = 29;
            this.label8.Text = "Total:";
            // 
            // Totallabel
            // 
            this.Totallabel.AutoSize = true;
            this.Totallabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Totallabel.Location = new System.Drawing.Point(266, 34);
            this.Totallabel.Name = "Totallabel";
            this.Totallabel.Size = new System.Drawing.Size(68, 32);
            this.Totallabel.TabIndex = 35;
            this.Totallabel.Text = "0.00";
            // 
            // Guardarbutton
            // 
            this.Guardarbutton.BackColor = System.Drawing.Color.Gray;
            this.Guardarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardarbutton.ForeColor = System.Drawing.Color.White;
            this.Guardarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Guardarbutton.Image")));
            this.Guardarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Guardarbutton.Location = new System.Drawing.Point(412, 300);
            this.Guardarbutton.Name = "Guardarbutton";
            this.Guardarbutton.Size = new System.Drawing.Size(147, 50);
            this.Guardarbutton.TabIndex = 36;
            this.Guardarbutton.Text = "Guardar";
            this.Guardarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Guardarbutton.UseVisualStyleBackColor = false;
            this.Guardarbutton.Click += new System.EventHandler(this.Guardarbutton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label2.Location = new System.Drawing.Point(10, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 22);
            this.label2.TabIndex = 39;
            this.label2.Text = "Venta al Contado";
            // 
            // VentaContado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(571, 362);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Guardarbutton);
            this.Controls.Add(this.Totallabel);
            this.Controls.Add(this.Exactobutton);
            this.Controls.Add(this.ValorRecibidotextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Cambiolabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ClientetextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ComentariotextBox);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VentaContado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentaContado";
            this.Load += new System.EventHandler(this.VentaContado_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Exactobutton;
        private System.Windows.Forms.TextBox ValorRecibidotextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Cambiolabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ClientetextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ComentariotextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Totallabel;
        private System.Windows.Forms.Button Guardarbutton;
        private System.Windows.Forms.Label label2;
    }
}