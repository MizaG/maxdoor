﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;
using PdfSharp;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace MaxDoor.Registros
{
    public partial class VentaContado : Form
    {
        public static double Total;
        public static int IdCliente;
        public static string NombreCliente;
        public bool ExactoPress = false;
        public static int IdVenta;
        private Pagos pagos;
        DataTable ProductosVenta;

        public VentaContado()
        {
            InitializeComponent();
            pagos = new Pagos();
        }

        public void CargarDatos()
        {
            Totallabel.Text = Total.ToString("N2");
            ClientetextBox.Tag = IdCliente;
            ClientetextBox.Text = NombreCliente;

            Ventas ventas = new Ventas();
            ProductosVenta = ventas.ListadoDetalle(IdVenta);
        }

        private bool LlenarDatos()
        {
            bool retorno = true;

            pagos.IdVenta = IdVenta;
            pagos.MontoTotal = Total;
            pagos.Comentario = ComentariotextBox.Text;
            pagos.IdUsuario = LogIn.IdUsuario;

            return retorno;
        }

        private void Exactobutton_Click(object sender, EventArgs e)
        {
            ExactoPress = true;
            if (Total > 0)
            {
                ValorRecibidotextBox.Text = Total.ToString("N2");
                Cambiolabel.Text = "0.00";
                ExactoPress = false;
            }
        }

        private void VentaContado_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void ValorRecibidotextBox_TextChanged(object sender, EventArgs e)
        {
            if (Total > 0 && ValorRecibidotextBox.Text.Length > 0)
            {
                if(ExactoPress != true)
                {
                    Cambiolabel.Text = (Convert.ToInt32(ValorRecibidotextBox.Text) - Total).ToString();
                }
            }
            else
            {
                Cambiolabel.Text = "0.00";
            }
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(ValorRecibidotextBox.Text == "" || Convert.ToDouble(ValorRecibidotextBox.Text) < Convert.ToDouble(Totallabel.Text))
            {
                MessageBox.Show("Digite un monto correcto!");
            }
            else
            {
                if (LlenarDatos())
                {
                    if (pagos.Insertar())
                    {
                        Ventas ventas = new Ventas();
                        if (ventas.CompletarVenta(IdVenta))
                        {
                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                            CrearPDF();
                        }

                    }
                }
            }
            

            
        }

        public void CrearPDF()
        {
            string TableProductos = "";
            for (int i = 0; i < ProductosVenta.Rows.Count; i++)
            {
                TableProductos += "<tr> <td class=\"td1\" > " + ProductosVenta.Rows[i][0].ToString() + " </td> <td>" + ProductosVenta.Rows[i][1].ToString() + "</td> <td class=\"td3-4\" > " + ProductosVenta.Rows[i][2].ToString() + "</td> <td class=\"td3-4\" > " + ProductosVenta.Rows[i][3].ToString() + "</td> </tr>";
            }

            string Comentario = "";
            if(ComentariotextBox.Text.Length > 0)
            {
                Comentario = "Comentario: " + ComentariotextBox.Text;
            }
            string html = "<style>.container { width:100%; font-family: arial, sans-serif; }table { font-family: arial, sans-serif; border-collapse: collapse; width: 95%; margin-left: 15px; margin-right: 15px; } th { background-color: darkgrey; border-radius: 5px; }td,th { border: 1px solid #dddddd; } .container2 { width:100%; height: 200px; }.div1 { float:left; margin-left: 15px; width:45%; }.div2 { text-align:center;  margin-right: auto; margin-left: auto; width:100%; }.div3 { width:100%; text-align: center; } .div4 { text-align:left; margin-left: 15px; margin-bottom: 35px; } .w18 { width:18%; } .w46 { width:46%; } .td1 { text-align:center; } .td3-4 { text-align:right; } .last-div { text-align: right; margin-right: 25px; } </style><div class=\"container\"> <div class=\"div2\" > <strong>Max Door<br>Calle Principal<br>809-246-2567<br>RNC: 16324656</strong></div> <div class=\"container2\" > <div class=\"div1\" ><img src=\"maxdoorlogo.png\" width =\"200px\" alt =\"\" ></div> </div>  <div class=\"div3\" ><h2>***FACTURA***</h2></div> <div class=\"div4\" ><strong>No: 00128<br> " + DateTime.Now.ToString("dd/MM/yyyy") + " <br>Hora: " + DateTime.Now.ToString("hh:mm tt") + " <br>Cajero: " + LogIn.NombreUsuario + " <br>Cliente: " + NombreCliente + " </strong></div> <table> <tr> <th class=\"w18\" > Cantidad</th> <th class=\"w46\" > Descripcion</th> <th class=\"w18\" > Precio</th> <th class=\"w18\" > Importe</th> </tr> " + TableProductos + " </table> <div class=\"last-div\" ><h3>TOTAL: " + Total.ToString("N2") + "</h3></div> <div style=\"margin-top:25px\">"+ Comentario +"</div> </div>";

            string path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\MaxDoor - Facturas y Contratos\\Facturas";
            Directory.CreateDirectory(path);
            PdfDocument pdf = PdfGenerator.GeneratePdf(html, PageSize.Letter);
            string DocName;
            DocName = "Factura " + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " + DateTime.Now.ToString("hh.mm.ss") + ".pdf";
            pdf.Save(path + "\\" + DocName);
            Process.Start(path + "\\" + DocName);
        }
    }
}
