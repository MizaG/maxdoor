﻿namespace MaxDoor.Registros
{
    partial class VentaCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentaCredito));
            this.label1 = new System.Windows.Forms.Label();
            this.AbonotextBox = new System.Windows.Forms.TextBox();
            this.Totallabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Restantelabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CantidadCuotastextBox = new System.Windows.Forms.TextBox();
            this.MontoXCuotastextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Exactobutton = new System.Windows.Forms.Button();
            this.ValorRecibidotextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Cambiolabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.DiaPagonumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.Guardarbutton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ClientetextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.MesescomboBox = new System.Windows.Forms.ComboBox();
            this.YearnumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ComentariotextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DiaPagonumericUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YearnumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 178);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Abono Inicial";
            // 
            // AbonotextBox
            // 
            this.AbonotextBox.Location = new System.Drawing.Point(130, 175);
            this.AbonotextBox.Name = "AbonotextBox";
            this.AbonotextBox.Size = new System.Drawing.Size(162, 27);
            this.AbonotextBox.TabIndex = 1;
            this.AbonotextBox.TextChanged += new System.EventHandler(this.AbonotextBox_TextChanged);
            this.AbonotextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AbonotextBox_KeyPress);
            // 
            // Totallabel
            // 
            this.Totallabel.AutoSize = true;
            this.Totallabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Totallabel.ForeColor = System.Drawing.Color.Black;
            this.Totallabel.Location = new System.Drawing.Point(357, 46);
            this.Totallabel.Name = "Totallabel";
            this.Totallabel.Size = new System.Drawing.Size(68, 32);
            this.Totallabel.TabIndex = 37;
            this.Totallabel.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(184, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 32);
            this.label8.TabIndex = 36;
            this.label8.Text = "Total:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 22);
            this.label2.TabIndex = 38;
            this.label2.Text = "Venta a Credito";
            // 
            // Restantelabel
            // 
            this.Restantelabel.AutoSize = true;
            this.Restantelabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Restantelabel.ForeColor = System.Drawing.Color.Red;
            this.Restantelabel.Location = new System.Drawing.Point(357, 78);
            this.Restantelabel.Name = "Restantelabel";
            this.Restantelabel.Size = new System.Drawing.Size(68, 32);
            this.Restantelabel.TabIndex = 40;
            this.Restantelabel.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(184, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 32);
            this.label4.TabIndex = 39;
            this.label4.Text = "Restante:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 21);
            this.label3.TabIndex = 41;
            this.label3.Text = "Cantidad de Cuotas";
            // 
            // CantidadCuotastextBox
            // 
            this.CantidadCuotastextBox.Location = new System.Drawing.Point(547, 175);
            this.CantidadCuotastextBox.Name = "CantidadCuotastextBox";
            this.CantidadCuotastextBox.Size = new System.Drawing.Size(100, 27);
            this.CantidadCuotastextBox.TabIndex = 42;
            this.CantidadCuotastextBox.TextChanged += new System.EventHandler(this.CantidadCuotastextBox_TextChanged);
            this.CantidadCuotastextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CantidadCuotastextBox_KeyPress);
            // 
            // MontoXCuotastextBox
            // 
            this.MontoXCuotastextBox.Enabled = false;
            this.MontoXCuotastextBox.Location = new System.Drawing.Point(547, 236);
            this.MontoXCuotastextBox.Name = "MontoXCuotastextBox";
            this.MontoXCuotastextBox.Size = new System.Drawing.Size(100, 27);
            this.MontoXCuotastextBox.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(373, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 19);
            this.label5.TabIndex = 43;
            this.label5.Text = "Monto por Cuotas = ";
            // 
            // Exactobutton
            // 
            this.Exactobutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.Exactobutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exactobutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exactobutton.ForeColor = System.Drawing.Color.White;
            this.Exactobutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exactobutton.Location = new System.Drawing.Point(130, 282);
            this.Exactobutton.Name = "Exactobutton";
            this.Exactobutton.Size = new System.Drawing.Size(145, 37);
            this.Exactobutton.TabIndex = 49;
            this.Exactobutton.Text = "Exacto";
            this.Exactobutton.UseVisualStyleBackColor = false;
            this.Exactobutton.Click += new System.EventHandler(this.Exactobutton_Click);
            // 
            // ValorRecibidotextBox
            // 
            this.ValorRecibidotextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValorRecibidotextBox.Location = new System.Drawing.Point(130, 246);
            this.ValorRecibidotextBox.Name = "ValorRecibidotextBox";
            this.ValorRecibidotextBox.Size = new System.Drawing.Size(145, 31);
            this.ValorRecibidotextBox.TabIndex = 45;
            this.ValorRecibidotextBox.TextChanged += new System.EventHandler(this.ValorRecibidotextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label6.Location = new System.Drawing.Point(126, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 22);
            this.label6.TabIndex = 48;
            this.label6.Text = "Valor Recibido:";
            // 
            // Cambiolabel
            // 
            this.Cambiolabel.AutoSize = true;
            this.Cambiolabel.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cambiolabel.ForeColor = System.Drawing.Color.Red;
            this.Cambiolabel.Location = new System.Drawing.Point(221, 326);
            this.Cambiolabel.Name = "Cambiolabel";
            this.Cambiolabel.Size = new System.Drawing.Size(48, 23);
            this.Cambiolabel.TabIndex = 47;
            this.Cambiolabel.Text = "0.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(124, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 23);
            this.label7.TabIndex = 46;
            this.label7.Text = "Cambio:";
            // 
            // DiaPagonumericUpDown
            // 
            this.DiaPagonumericUpDown.Location = new System.Drawing.Point(56, 112);
            this.DiaPagonumericUpDown.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.DiaPagonumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DiaPagonumericUpDown.Name = "DiaPagonumericUpDown";
            this.DiaPagonumericUpDown.Size = new System.Drawing.Size(55, 27);
            this.DiaPagonumericUpDown.TabIndex = 50;
            this.DiaPagonumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(117, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 21);
            this.label10.TabIndex = 52;
            this.label10.Text = "de cada mes";
            // 
            // Guardarbutton
            // 
            this.Guardarbutton.BackColor = System.Drawing.Color.Gray;
            this.Guardarbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardarbutton.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardarbutton.ForeColor = System.Drawing.Color.White;
            this.Guardarbutton.Image = ((System.Drawing.Image)(resources.GetObject("Guardarbutton.Image")));
            this.Guardarbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Guardarbutton.Location = new System.Drawing.Point(511, 443);
            this.Guardarbutton.Name = "Guardarbutton";
            this.Guardarbutton.Size = new System.Drawing.Size(147, 50);
            this.Guardarbutton.TabIndex = 53;
            this.Guardarbutton.Text = "Guardar";
            this.Guardarbutton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Guardarbutton.UseVisualStyleBackColor = false;
            this.Guardarbutton.Click += new System.EventHandler(this.Guardarbutton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label11.Location = new System.Drawing.Point(50, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 22);
            this.label11.TabIndex = 56;
            this.label11.Text = "Cliente";
            // 
            // ClientetextBox
            // 
            this.ClientetextBox.Enabled = false;
            this.ClientetextBox.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientetextBox.Location = new System.Drawing.Point(130, 138);
            this.ClientetextBox.Name = "ClientetextBox";
            this.ClientetextBox.Size = new System.Drawing.Size(517, 31);
            this.ClientetextBox.TabIndex = 55;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(390, 199);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(129, 19);
            this.label12.TabIndex = 57;
            this.label12.Text = "(Monto Restante)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(165, 21);
            this.label13.TabIndex = 58;
            this.label13.Text = "A partir del mes de: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 21);
            this.label14.TabIndex = 59;
            this.label14.Text = "Dia: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.MesescomboBox);
            this.groupBox1.Controls.Add(this.YearnumericUpDown);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.DiaPagonumericUpDown);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(328, 269);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 168);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fecha de Pago";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 21);
            this.label15.TabIndex = 62;
            this.label15.Text = "del año";
            // 
            // MesescomboBox
            // 
            this.MesescomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MesescomboBox.FormattingEnabled = true;
            this.MesescomboBox.Location = new System.Drawing.Point(160, 26);
            this.MesescomboBox.Name = "MesescomboBox";
            this.MesescomboBox.Size = new System.Drawing.Size(164, 29);
            this.MesescomboBox.TabIndex = 60;
            // 
            // YearnumericUpDown
            // 
            this.YearnumericUpDown.Location = new System.Drawing.Point(81, 57);
            this.YearnumericUpDown.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.YearnumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.YearnumericUpDown.Name = "YearnumericUpDown";
            this.YearnumericUpDown.Size = new System.Drawing.Size(55, 27);
            this.YearnumericUpDown.TabIndex = 61;
            this.YearnumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.label9.Location = new System.Drawing.Point(16, 383);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 21);
            this.label9.TabIndex = 62;
            this.label9.Text = "Comentario:";
            // 
            // ComentariotextBox
            // 
            this.ComentariotextBox.Location = new System.Drawing.Point(16, 407);
            this.ComentariotextBox.MaxLength = 150;
            this.ComentariotextBox.Multiline = true;
            this.ComentariotextBox.Name = "ComentariotextBox";
            this.ComentariotextBox.Size = new System.Drawing.Size(277, 83);
            this.ComentariotextBox.TabIndex = 61;
            // 
            // VentaCredito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(670, 505);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ComentariotextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ClientetextBox);
            this.Controls.Add(this.Guardarbutton);
            this.Controls.Add(this.Exactobutton);
            this.Controls.Add(this.ValorRecibidotextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Cambiolabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MontoXCuotastextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CantidadCuotastextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Restantelabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Totallabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.AbonotextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(1)))), ((int)(((byte)(102)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "VentaCredito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentaCredito";
            this.Load += new System.EventHandler(this.VentaCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DiaPagonumericUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YearnumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AbonotextBox;
        private System.Windows.Forms.Label Totallabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Restantelabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CantidadCuotastextBox;
        private System.Windows.Forms.TextBox MontoXCuotastextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Exactobutton;
        private System.Windows.Forms.TextBox ValorRecibidotextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Cambiolabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown DiaPagonumericUpDown;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button Guardarbutton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ClientetextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox MesescomboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ComentariotextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown YearnumericUpDown;
    }
}