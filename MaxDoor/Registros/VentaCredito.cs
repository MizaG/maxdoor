﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modelos;
using PdfSharp;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace MaxDoor.Registros
{
    public partial class VentaCredito : Form
    {
        public static int IdCliente;
        public static string NombreCliente;
        public static double Total;
        public static int IdVenta;
        bool ExactoPress;
        private Cuotas cuotas;
        private Pagos pagos;
        DataTable ProductosVenta;

        public VentaCredito()
        {
            InitializeComponent();
            AgregarMeses();
            cuotas = new Cuotas();
            pagos = new Pagos();
        }

        public void CargarDatos()
        {
            Totallabel.Text = Total.ToString("N2");
            ClientetextBox.Tag = IdCliente;
            ClientetextBox.Text = NombreCliente;
            Restantelabel.Text = Total.ToString("N2");

            Ventas ventas = new Ventas();
            ProductosVenta = ventas.ListadoDetalle(IdVenta);
        }

        public void AgregarMeses()
        {
            Dictionary<string, string> Meses = new Dictionary<string, string>();

            Meses.Add("1", "Enero");
            Meses.Add("2", "Febrero");
            Meses.Add("3", "Marzo");
            Meses.Add("4", "Abril");
            Meses.Add("5", "Mayo");
            Meses.Add("6", "Junio");
            Meses.Add("7", "Julio");
            Meses.Add("8", "Agosto");
            Meses.Add("9", "Septiembre");
            Meses.Add("10", "Octubre");
            Meses.Add("11", "Noviembre");
            Meses.Add("12", "Diciembre");

            MesescomboBox.DataSource = new BindingSource(Meses, null);
            MesescomboBox.DisplayMember = "Value";
            MesescomboBox.ValueMember = "Key";
        }

        private void VentaCredito_Load(object sender, EventArgs e)
        {
            CargarDatos();
            MesescomboBox.SelectedIndex = DateTime.Now.Month;
            DiaPagonumericUpDown.Value = DateTime.Now.Day;
            YearnumericUpDown.Minimum = DateTime.Now.Year;
            YearnumericUpDown.Value = DateTime.Now.Year;
            

        }

        private void ValorRecibidotextBox_TextChanged(object sender, EventArgs e)
        {
            if (AbonotextBox.Text.Length > 0 && Convert.ToDouble(AbonotextBox.Text) > 0 && ValorRecibidotextBox.Text.Length > 0)
            {
                if (ExactoPress != true)
                {
                    Cambiolabel.Text = (Convert.ToInt32(ValorRecibidotextBox.Text) - Convert.ToDouble(AbonotextBox.Text)).ToString();
                }
            }
            else
            {
                Cambiolabel.Text = "0.00";
            }
        }

        private void Exactobutton_Click(object sender, EventArgs e)
        {
            ExactoPress = true;
            if (AbonotextBox.Text.Length > 0 && Convert.ToDouble(AbonotextBox.Text) > 0)
            {
                ValorRecibidotextBox.Text = Convert.ToDouble(AbonotextBox.Text).ToString("N2");
                Cambiolabel.Text = "0.00";
                ExactoPress = false;
            }
        }

        private void AbonotextBox_TextChanged(object sender, EventArgs e)
        {
            if (AbonotextBox.Text.Length > 0 && Convert.ToDouble(AbonotextBox.Text) > 0)
            {
                Restantelabel.Text = (Total - Convert.ToDouble(AbonotextBox.Text)).ToString("N2");
            }
            else
            {
                Restantelabel.Text = Total.ToString("N2");
            }

        }

        private void CantidadCuotastextBox_TextChanged(object sender, EventArgs e)
        {
            if (CantidadCuotastextBox.Text.Length > 0 && Convert.ToInt32(CantidadCuotastextBox.Text) > 0)
            {
                double MontoXCuotas;
                MontoXCuotas = Convert.ToDouble(Restantelabel.Text) / Convert.ToInt32(CantidadCuotastextBox.Text);
                MontoXCuotastextBox.Text = MontoXCuotas.ToString("N2");
            }
            else
            {
                MontoXCuotastextBox.Text = "";
            }

        }

        private bool LlenarDatosPago()
        {
            bool retorno = true;

            pagos.IdVenta = IdVenta;
            pagos.MontoTotal = Convert.ToDouble(AbonotextBox.Text);
            pagos.Comentario = ComentariotextBox.Text;
            pagos.IdUsuario = LogIn.IdUsuario;

            return retorno;
        }

        private bool LlenarDatos(int NumeroCuota, string FechaCuota)
        {
            bool retorno = true;

            cuotas.NumeroCuota = NumeroCuota;
            cuotas.IdVenta = IdVenta;
            cuotas.FechaCuota = FechaCuota;
            cuotas.Monto = Convert.ToDouble(MontoXCuotastextBox.Text);
            cuotas.Completada = false;
            cuotas.Balance = Convert.ToDouble(MontoXCuotastextBox.Text);

            return retorno;
        }

        private void Guardarbutton_Click(object sender, EventArgs e)
        {
            if(AbonotextBox.Text != "" && MontoXCuotastextBox.Text != "")
            {
                int Mes = Convert.ToInt32(MesescomboBox.SelectedValue);
                int Year = DateTime.Now.Year;
                string FechaCompleta;
                string FullDate;
                string Hoy;
                int ValFecha;
                try
                {
                    FullDate = DiaPagonumericUpDown.Value.ToString() + "/" + Mes.ToString() + "/" + Year.ToString();
                    Hoy = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + Year;
                    ValFecha = DateTime.Compare(DateTime.Parse(FullDate), DateTime.Parse(Hoy));
                }
                catch (Exception)
                {
                    FullDate = Mes.ToString() + "/" + DiaPagonumericUpDown.Value.ToString() + "/" + Year.ToString();
                    Hoy = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + Year;
                    ValFecha = DateTime.Compare(DateTime.Parse(FullDate), DateTime.Parse(Hoy));
                }




                if (ValFecha < 0)
                {
                    MessageBox.Show("La fecha de inicio no puede ser menor que la fecha de hoy", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (LlenarDatosPago())
                    {
                        if (pagos.Insertar())
                        {
                            for (int i = 1; i <= Convert.ToInt32(CantidadCuotastextBox.Text); i++)
                            {
                                int Dia = Convert.ToInt32(DiaPagonumericUpDown.Value);
                                if (Mes == 2 && Dia > 28)
                                {
                                    Dia = 28;
                                }
                                else if (Mes == 4 && Dia > 30)
                                {
                                    Dia = 30;
                                }
                                else if (Mes == 6 && Dia > 30)
                                {
                                    Dia = 30;
                                }
                                else if (Mes == 9 && Dia > 30)
                                {
                                    Dia = 30;
                                }
                                else if (Mes == 11 && Dia > 30)
                                {
                                    Dia = 30;
                                }

                                FechaCompleta = Year.ToString() + "-" + Mes.ToString() + "-" + Dia.ToString();

                                if (LlenarDatos(i, FechaCompleta))
                                {
                                    cuotas.Insertar();

                                }

                                if (Mes == 12)
                                {
                                    Year++;
                                    Mes = 1;
                                }
                                else
                                {
                                    Mes++;
                                }
                            }

                            pagos.ActualizarCantidadCuotas(IdVenta, Convert.ToInt32(CantidadCuotastextBox.Text));
                            pagos.ActualizarBalance(IdVenta, Convert.ToDouble(AbonotextBox.Text));

                            MessageBox.Show("Guardado Correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                            CrearPDF();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Digite los datos faltantes!");
            }
            
        }

        public void CrearPDF()
        {
            string TableProductos = "";
            for (int i = 0; i < ProductosVenta.Rows.Count; i++)
            {
                TableProductos += "<tr> <td class=\"td1\" > " + ProductosVenta.Rows[i][0].ToString() + " </td> <td>" + ProductosVenta.Rows[i][1].ToString() + "</td> <td class=\"td3-4\" > " + ProductosVenta.Rows[i][2].ToString() + "</td> <td class=\"td3-4\" > " + ProductosVenta.Rows[i][3].ToString() + "</td> </tr>";
            }

            Ventas ventas = new Ventas();
            string path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\MaxDoor - Facturas y Contratos\\Contratos";
            Directory.CreateDirectory(path);

            string Comentario = "";
            if (ComentariotextBox.Text.Length > 0)
            {
                Comentario = "Comentario: " + ComentariotextBox.Text;
            }

            string html = "<style>.container { width:100%; font-family: arial, sans-serif; }table { font-family: arial, sans-serif; border-collapse: collapse; width: 95%; } th { background-color: darkgrey; border-radius: 5px; }td,th { border: 1px solid #dddddd; } </style><div class=\"container\"> <div style=\"width: 100 %; height: 100px; \"> <div style=\"float:right; text-align:center; margin-right: auto; width: 100%; \" > <strong>Max Door<br>Calle Principal / 809-246-2567 / RNC: 16324656</strong></div> <div style=\"float:left; margin-left: 15px; width: 45 %; \" ><img src=\"maxdoorlogo.png\" width =\"125px\" alt =\"\" ></div>  </div> <div style=\"width: 100 %; text-align: center; \" ><h2>***CONTRATO***</h2></div> <div style=\"text-align:left; margin-left: 15px; margin-bottom: 35px; \" ><strong>No: " + ventas.MaximoId() +"<br> "+ DateTime.Now.ToString("dd/MM/yyyy") +" <br>Hora: "+ DateTime.Now.ToString("HH:mm:ss") + " <br>Cajero: "+ LogIn.NombreUsuario +" <br>Cliente: "+ NombreCliente +"</strong></div> <table style=\"margin-left: 15px; margin-right: 15px; \" > <tr> <th style=\"width: 18 % \" > Cantidad</th> <th style=\"width: 46 % \" > Descripcion</th> <th style=\"width: 18 % \" > Precio</th> <th style=\"width: 18 % \" > Importe</th> </tr> " + TableProductos+ " </table><br> <div style=\"text-align: right; margin-right: 25px; width: 100 %; float: right; \" ><strong><label style=\"margin-right:30px\">TOTAL: "+ Totallabel.Text +" </label><label style=\"margin-right:30px\"> Abono: " + AbonotextBox.Text + " </label><label> Restante: "+ Restantelabel.Text + " </label></strong></div> <div style=\"margin-top:25px\">" + Comentario + "</div> <p ALIGN=\"justify\" style =\"font-size: 12px; \" > Incluye: <br> <b>Instalacion<br> Terminacion</b><br><br> <b>El Cliente debe Suministrar:</b> <br> El Area libre de obstaculos para el buen desenvolvimiento del trabajo. <br> Supervisor de obra que apruebe de manera diaria las instalaciones. <br><br> <b>Forma de Pago:</b><br> Abono de " + AbonotextBox.Text +" mientras que el restante sera pagado en "+ CantidadCuotastextBox.Text +" Cuotas por un monto de "+ MontoXCuotastextBox.Text +" cada una. <br><br> La aprobacion de la cotizacion, mediante la firma DEL CONTRATANTE, Implica automaticamente la formal ORDEN DE TRABAJO y su consecuencia adhesion a los terminos y especificaciones contenidas en la misma. El cumplimiento de la compañia, salvo caso fortuito de fuerza mayor o causa atendible, estara directamente relacionado con los desembolsos estipulados en el presente documento con el cargo AL CONTRATANTE, el cual se compromete a cumplir fielmente con dichos pagos. En caso de incumplimiento de estas obligaciones. LA COMPAÑIA se reserva todos los derechos y acciones que la ley le confiere a los fines de obtener los pagos de la forma establecida en este documento.</p> <br> <div style=\"width:100%\"> <div style=\"text-align: center; width: 30%; float: left; \" > ________________________________<br>Danny Abreu Jimenez - Gerente</div><br> <div style=\"text-align: center; width: 30%; float: right; \" > ________________________________<br>Contratante<br><br>________________________________<br>No. Cedula</div></div> </div>";
            string htmlcopia = "<style>.container { width:100%; font-family: arial, sans-serif; }table { font-family: arial, sans-serif; border-collapse: collapse; width: 95%; } th { background-color: darkgrey; border-radius: 5px; }td,th { border: 1px solid #dddddd; } </style><div class=\"container\"> <div style=\"width: 100 %; height: 100px; \"> <div style=\"float:right; text-align:center; margin-right: auto; width: 100%; \" > <strong>Max Door<br>Calle Principal / 809-246-2567 / RNC: 16324656</strong></div> <div style=\"float:left; margin-left: 15px; width: 45 %; \" ><img src=\"maxdoorlogo.png\" width =\"125px\" alt =\"\" ></div>  </div> <div style=\"width: 100 %; text-align: center; \" ><h2>***COPIA CONTRATO***</h2></div> <div style=\"text-align:left; margin-left: 15px; margin-bottom: 35px; \" ><strong>No: " + ventas.MaximoId() + "<br> " + DateTime.Now.ToString("dd/MM/yyyy") + " <br>Hora: " + DateTime.Now.ToString("HH:mm:ss") + " <br>Cajero: " + LogIn.NombreUsuario + " <br>Cliente: " + NombreCliente + "</strong></div> <table style=\"margin-left: 15px; margin-right: 15px; \" > <tr> <th style=\"width: 18 % \" > Cantidad</th> <th style=\"width: 46 % \" > Descripcion</th> <th style=\"width: 18 % \" > Precio</th> <th style=\"width: 18 % \" > Importe</th> </tr> " + TableProductos + " </table><br> <div style=\"text-align: right; margin-right: 25px; width: 100 %; float: right; \" ><strong><label style=\"margin-right:30px\">TOTAL: " + Totallabel.Text + " </label><label style=\"margin-right:30px\"> Abono: " + AbonotextBox.Text + " </label><label> Restante: " + Restantelabel.Text + " </label></strong></div> <div style=\"margin-top:25px\">" + Comentario + "</div> <p ALIGN=\"justify\" style =\"font-size: 12px; \" > Incluye: <br> <b>Instalacion<br> Terminacion</b><br><br> <b>El Cliente debe Suministrar:</b> <br> El Area libre de obstaculos para el buen desenvolvimiento del trabajo. <br> Supervisor de obra que apruebe de manera diaria las instalaciones. <br><br> <b>Forma de Pago:</b><br> Abono de " + AbonotextBox.Text + " mientras que el restante sera pagado en " + CantidadCuotastextBox.Text + " Cuotas por un monto de " + MontoXCuotastextBox.Text + " cada una. <br><br> La aprobacion de la cotizacion, mediante la firma DEL CONTRATANTE, Implica automaticamente la formal ORDEN DE TRABAJO y su consecuencia adhesion a los terminos y especificaciones contenidas en la misma. El cumplimiento de la compañia, salvo caso fortuito de fuerza mayor o causa atendible, estara directamente relacionado con los desembolsos estipulados en el presente documento con el cargo AL CONTRATANTE, el cual se compromete a cumplir fielmente con dichos pagos. En caso de incumplimiento de estas obligaciones. LA COMPAÑIA se reserva todos los derechos y acciones que la ley le confiere a los fines de obtener los pagos de la forma establecida en este documento.</p> <br> <div style=\"width:100%\"> <div style=\"text-align: center; width: 30%; float: left; \" > ________________________________<br>Danny Abreu Jimenez - Gerente</div><br> <div style=\"text-align: center; width: 30%; float: right; \" > ________________________________<br>Contratante<br><br>________________________________<br>No. Cedula</div></div> </div>";

            PdfDocument pdf = PdfGenerator.GeneratePdf(html, PageSize.Letter);
            PdfDocument pdfcopia = PdfGenerator.GeneratePdf(htmlcopia, PageSize.Letter);
            string DocName;
            DocName = "Contrato " + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + " " + DateTime.Now.ToString("hh.mm.ss") + ".pdf";
            pdf.Save(path + "\\" + DocName);
            Process.Start(path + "\\" + DocName);

            pdfcopia.Save(path + "\\" + "Copia - " + DocName);
            Process.Start(path + "\\" + "Copia - " + DocName);
        }

        private void AbonotextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar.ToString().Equals("."))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void CantidadCuotastextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
