﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxDoor.VentanaReportes
{
    public partial class VentaReportePagos : Form
    {
        Reportes.ReportePagos reportePagos = new Reportes.ReportePagos();
        public VentaReportePagos()
        {
            InitializeComponent();
        }

        private void Buscarbutton_Click(object sender, EventArgs e)
        {
            reportePagos.SetParameterValue("@Desde", DesdedateTimePicker.Value.ToShortDateString() + " 00:00:00 AM");
            reportePagos.SetParameterValue("@Hasta", HastadateTimePicker.Value.ToString());
            //rpt.SetDatabaseLogon("Adap", "*Adap123456789");
            crystalReportViewer1.ReportSource = reportePagos;
        }

        private void VentaReportePagos_Load(object sender, EventArgs e)
        {
            reportePagos.SetParameterValue("@Desde", DateTime.Today);
            reportePagos.SetParameterValue("@Hasta", DateTime.Now);
            //rpt.SetDatabaseLogon("Adap", "*Adap123456789");
            crystalReportViewer1.ReportSource = reportePagos;
        }
    }
}
