﻿using Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxDoor.VentanaReportes
{
    public partial class VentanaReporteVentas : Form
    {
        Reportes.ReporteVentas reporteVentas = new Reportes.ReporteVentas();
        public VentanaReporteVentas()
        {
            InitializeComponent();
            
        }

        private void VentanaReporteVentas_Load(object sender, EventArgs e)
        {
            reporteVentas.SetParameterValue("@Desde", DateTime.Today);
            reporteVentas.SetParameterValue("@Hasta", DateTime.Now);
            //rpt.SetDatabaseLogon("Adap", "*Adap123456789");
            crystalReportViewer1.ReportSource = reporteVentas;
        }

        private void Buscarbutton_Click(object sender, EventArgs e)
        {
            reporteVentas.SetParameterValue("@Desde", DesdedateTimePicker.Value.ToShortDateString() + " 00:00:00 AM");
            reporteVentas.SetParameterValue("@Hasta", HastadateTimePicker.Value.ToString());
            //rpt.SetDatabaseLogon("Adap", "*Adap123456789");
            crystalReportViewer1.ReportSource = reporteVentas;
        }
    }
}
