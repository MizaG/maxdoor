﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Clientes
    {

        public int IdCLiente { get; set; }
        public string Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public bool Activo { get; set; }
        ConexionDB conexion = new ConexionDB();

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Clientes(Cedula,Nombres,Apellidos,Direccion,Telefono)" +
                    " Values('{0}','{1}','{2}','{3}','{4}')", this.Cedula, this.Nombres, this.Apellidos, this.Direccion, this.Telefono));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Clientes where " + Condicion + " " + ordenFinal);
        }

        public bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select * from Clientes where IdCliente = "+ IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdCLiente = (int)dtVentas.Rows[0]["IdCliente"];
                    this.Cedula = dtVentas.Rows[0]["Cedula"].ToString();
                    this.Nombres = dtVentas.Rows[0]["Nombres"].ToString();
                    this.Apellidos = dtVentas.Rows[0]["Apellidos"].ToString();
                    this.Direccion = dtVentas.Rows[0]["Direccion"].ToString();
                    this.Telefono = dtVentas.Rows[0]["Telefono"].ToString();

                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Editar(int Id)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Clientes set Cedula = '{0}', Nombres = '{1}', Apellidos = '{2}', Direccion = '{3}', Telefono = '{4}' where IdCliente = {5}", this.Cedula, this.Nombres, this.Apellidos, this.Direccion, this.Telefono, Id));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }
    }
}
