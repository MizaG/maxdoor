﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Compras
    {
        public int IdCompra { get; set; }
        public int IdProveedor { get; set; }
        public int IdUsuario { get; set; }
        public double ITBIS { get; set; }
        public string NCF { get; set; }
        public string FechaYHora { get; set; }
        public double MontoTotal { get; set; }
        public bool Activo { get; set; }
        public int IdProducto { get; set; }
        public double Precio { get; set; }
        public double Cantidad { get; set; }
        public double Importe { get; set; }
        public List<Productos> productos { get; set; }
        ConexionDB conexion = new ConexionDB(); 

        public Compras()
        {
            this.productos = new List<Productos>();
        }
        public void LimpiarList()
        {
            this.productos.Clear();
        }

        public void AgregarProductos(int IdProducto, double Cantidad, double Precio, double Importe)
        {
            this.productos.Add(new Productos(IdProducto, Cantidad, Precio, Importe));

        }

        public bool Insertar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Compras (IdProveedor,IdUsuario,NCF, FechaYHora,MontoTotal)" +
                    "Values ({0},{1},'{2}',GETDATE(),{3});",
                                            this.IdProveedor, this.IdUsuario, this.NCF, this.MontoTotal));
                if (retorno)
                {
                    this.IdCompra = (int)conexion.ObtenerDatos(String.Format("select MAX(IdCompra) as IdCompra from Compras")).Rows[0]["IdCompra"];
                    foreach (var pro in productos)
                    {
                        comando.AppendLine(String.Format("insert into ComprasDetalle(IdProducto,IdCompra, Precio, Cantidad,Importe) values({0},{1},{2},{3},{4})", pro.IdProducto, this.IdCompra, pro.Precio, pro.Cantidad, pro.Importe));
                    }
                }

                retorno = conexion.Ejecutar(comando.ToString());
            }
            catch (Exception e)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable ListadoCompras(string Filtro)
        {
            ConexionDB con = new ConexionDB();
            return con.ObtenerDatos("select com.NCF, pro.Empresa, com.FechaYHora, com.MontoTotal, u.Nombres + ' ' + u.Apellidos as 'Registrado por' from Compras com inner join Proveedores pro on com.IdProveedor = pro.IdProveedor inner join Usuarios u on com.IdUsuario = u.IdUsuario where pro.Empresa like '%" + Filtro + "%' and com.Activo = 1 order by com.FechaYHora desc");
        }
    }
}
