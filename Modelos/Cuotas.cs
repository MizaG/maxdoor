﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Cuotas
    {
        public int IdCuota { get; set; }
        public int NumeroCuota { get; set; }
        public int IdVenta { get; set; }
        public string FechaCuota { get; set; }
        public double Monto { get; set; }
        public bool Completada { get; set; }
        public bool Activo { get; set; }
        public double Balance { get; set; }
        ConexionDB conexion = new ConexionDB();

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Cuotas(NumeroCuota,IdVenta,FechaCuota,Monto,Completada,Balance)" +
                    " Values({0},{1},'{2}',{3},'{4}',{5})", this.NumeroCuota, this.IdVenta, this.FechaCuota, this.Monto, this.Completada, this.Balance));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Cuotas where " + Condicion + " " + ordenFinal);
        }

        public DataTable ListadoDashboard(string CondicionFecha)
        {
            ConexionDB con = new ConexionDB();
            return con.ObtenerDatos("select cl.Nombres + ' ' + cl.Apellidos as 'Cliente', c.FechaCuota as 'Fecha de Pago', c.Balance, c.NumeroCuota, cl.Telefono, v.MontoTotal as 'Total Venta' from Cuotas c inner join Ventas v on c.IdVenta = v.IdVenta inner join Clientes cl on cl.IdCliente = v.IdCliente where c.Completada = 0 " + CondicionFecha);
        }

        public bool BuscarProxCuota(int IdVenta)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select top 1 IdCuota, NumeroCuota, Balance from Cuotas where IdVenta = "+IdVenta+" and Completada = 0 and Activo = 1 order by NumeroCuota"));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdCuota = (int)dtVentas.Rows[0]["IdCuota"];
                    this.NumeroCuota = (int)dtVentas.Rows[0]["NumeroCuota"];
                    this.Balance = (double)dtVentas.Rows[0]["Balance"];

                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool CompletarCuota(double Pago, int IdCuota)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Cuotas set Balance = Balance -" + Pago + ", Completada = 1 where IdCuota = {0}", IdCuota));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool ActualizarBalance(double Pago, int IdCuota)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Cuotas set Balance = Balance -" + Pago + " where IdCuota = {0}", IdCuota));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

    }
}
