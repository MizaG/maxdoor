﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Inventarios
    {
        public int IdInventario { get; set; }
        public int IdProducto { get; set; }
        public double Existencia { get; set; }
        public string NombreProducto { get; set; }

        public bool Buscar(int IdBuscado)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                DataTable dtInventario = new DataTable();
                dtInventario = conexion.ObtenerDatos(string.Format("select p.Nombre, i.Existencia from Inventario i, Productos p where i.IdProducto = p.IdProducto and i.IdProducto = {0}", IdBuscado));
                if (dtInventario.Rows.Count > 0)
                {
                    this.NombreProducto = dtInventario.Rows[0]["Nombre"].ToString();
                    this.Existencia = (double)dtInventario.Rows[0]["Existencia"];
                    retorno = true;
                }
            }
            catch (Exception e)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Insertar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Inventario(IdProducto,Existencia) values({0},{1})", this.IdProducto, this.Existencia));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Editar()
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Inventario set Existencia = {0} where IdProducto = {1}", this.Existencia, this.IdProducto));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Inventario i, Productos p where " + Condicion + " " + ordenFinal);
        }

        public bool ActualizarInventario(int Idprod, string Condicion, double Cantidad)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Inventario set Existencia = Existencia "+ Condicion + " " + Cantidad +"  where IdProducto = " + Idprod));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }
    }
}
