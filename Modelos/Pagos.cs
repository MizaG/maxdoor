﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Pagos
    {
        public int IdPago { get; set; }
        public string Fecha { get; set; }
        public int IdVenta { get; set; }
        public double MontoTotal { get; set; }
        public string Comentario { get; set; }
        public bool Activo { get; set; }
        public int IdUsuario { get; set; }
        ConexionDB conexion = new ConexionDB();
        

        public int NumeroCuota { get; set; }
        public double MontoDetalle { get; set; }

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Pagos(Fecha,IdVenta,MontoTotal,Comentario, IdUsuario)" +
                    " Values(GETDATE(),{0},{1},'{2}', {3})", this.IdVenta, this.MontoTotal, this.Comentario, this.IdUsuario));

            }
            catch (Exception e)
            {
                retorno = false;
            }

            return retorno;
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(IdPago) as IdPago from Pagos")).Rows[0]["IdPago"];
        }

        public bool InsertarDetalle()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into PagosDetalle(IdPago,NumeroCuota,Monto)" +
                    " Values({0},{1},{2})", this.IdPago, this.NumeroCuota, this.MontoDetalle));

            }
            catch (Exception e)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool ActualizarCantidadCuotas(int IdVenta, double Balance)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set Balance = " + Balance + " where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool ActualizarCantidadCuotas(int IdVenta, int CantCuotas)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set CantidadCuotas = "+ CantCuotas +" where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool ActualizarBalance(int IdVenta, double Pago)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set Balance = Balance -" + Pago + " where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }
    }
}
