﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Productos
    {
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public double Precio { get; set; }
        public double Costo { get; set; }
        public int IdTipoProducto { get; set; }
        public int IdProveedor { get; set; }
        public string UnidadMedida { get; set; }
        public bool Activo { get; set; }
        public double Cantidad { get; set; }
        public double Importe { get; set; }
        public double PrecioMinimo { get; set; }

        public Productos()
        {

        }

        public Productos(int IdProducto, double Cantidad, double Precio, double Importe)
        {
            this.IdProducto = IdProducto;
            this.Cantidad = Cantidad;
            this.Precio = Precio;
            this.Importe = Importe;
        }

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                ConexionDB con = new ConexionDB();
                retorno = con.Ejecutar(String.Format("Insert into Productos(Nombre,Precio,Costo,IdTipoProducto,IdProveedor, UnidadMedida, PrecioMinimo)" +
                    " Values('{0}',{1},{2},{3},{4},'{5}', {6})", this.Nombre, this.Precio, this.Costo, this.IdTipoProducto, this.IdProveedor, this.UnidadMedida, this.PrecioMinimo));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Productos where " + Condicion + " " + ordenFinal);
        }

        public DataTable ListadoInventario()
        {
            ConexionDB con = new ConexionDB();
            return con.ObtenerDatos("select IdProducto, p.Nombre, tp.TipoProducto as 'Tipo', p.UnidadMedida as 'Medida', p.Precio, p.Costo from Productos p inner join TipoProductos tp on p.IdTipoProducto = tp.IdTipoProducto where Activo = 1;");
        }

        public DataTable ListadoTipoProductos(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from TipoProductos where " + Condicion + " " + ordenFinal);
        }

        public bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select * from Productos where IdProducto = " + IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdProducto = (int)dtVentas.Rows[0]["IdProducto"];
                    this.Nombre = dtVentas.Rows[0]["Nombre"].ToString();
                    this.Precio = (double)dtVentas.Rows[0]["Precio"];
                    this.Costo = (double)dtVentas.Rows[0]["Costo"];
                    this.IdTipoProducto = (int)dtVentas.Rows[0]["IdTipoProducto"];
                    this.IdProveedor = (int)dtVentas.Rows[0]["IdProveedor"];
                    this.UnidadMedida = dtVentas.Rows[0]["UnidadMedida"].ToString();
                    this.PrecioMinimo = (double)dtVentas.Rows[0]["PrecioMinimo"];

                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Editar(int Id)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Productos set Nombre = '{0}', Precio = {1}, Costo = {2}, IdTipoProducto = {3}, IdProveedor = {4}, UnidadMedida = '{5}', PrecioMinimo = {6} where IdProducto = {7}", this.Nombre, this.Precio, this.Costo, this.IdTipoProducto, this.IdProveedor, this.UnidadMedida, this.PrecioMinimo, Id));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(IdProducto) as IdProducto from Productos")).Rows[0]["IdProducto"];
        }
    }
}
