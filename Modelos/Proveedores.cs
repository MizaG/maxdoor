﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Proveedores
    {
        public int IdProveedor { get; set; }
        public string Empresa { get; set; }
        public string Representante { get; set; }
        public string RNC { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public bool Activo { get; set; }
        ConexionDB conexion = new ConexionDB();

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("insert into Proveedores(Empresa,Representante,RNC,Direccion,Telefono,Celuluar,Correo) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                                                this.Empresa, this.Representante, this.RNC, this.Direccion, this.Telefono, this.Celular, this.Correo));
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Proveedores where " + Condicion + " " + ordenFinal);
        }

        public bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select * from Proveedores where IdProveedor = " + IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdProveedor = (int)dtVentas.Rows[0]["IdProveedor"];
                    this.Empresa = dtVentas.Rows[0]["Empresa"].ToString();
                    this.Representante = dtVentas.Rows[0]["Representante"].ToString();
                    this.RNC = dtVentas.Rows[0]["RNC"].ToString();
                    this.Direccion = dtVentas.Rows[0]["Direccion"].ToString();
                    this.Telefono = dtVentas.Rows[0]["Telefono"].ToString();
                    this.Celular = dtVentas.Rows[0]["Celuluar"].ToString();
                    this.Correo = dtVentas.Rows[0]["Correo"].ToString();

                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Editar(int Id)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Proveedores set Empresa = '{0}', Representante = '{1}', RNC = '{2}', Direccion = '{3}', Telefono = '{4}', Celuluar = '{5}', Correo = '{6}' where IdProveedor = {7}", this.Empresa, this.Representante, this.RNC, this.Direccion, this.Telefono, this.Celular, this.Correo, Id));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }
    }
}
