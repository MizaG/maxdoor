﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreUsuario { get; set; }
        public string Pass { get; set; }
        public int Rol { get; set; }
        public string FechaCreacion { get; set; }
        public bool Activo { get; set; }
        ConexionDB conexion = new ConexionDB();

        public bool Insertar()
        {
            bool retorno = false;

            try
            {
                retorno = conexion.Ejecutar(String.Format("Insert into Usuarios(Nombres,Apellidos,NombreUsuario,Pass,Rol,FechaCreacion)" +
                    " Values('{0}','{1}','{2}','{3}','{4}', GETDATE())", this.Nombres, this.Apellidos, this.NombreUsuario, this.Pass, this.Rol));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Buscar(string Usuario, string Pass)
        {
            DataTable dt = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            //try
            //{
                dt = conexion.ObtenerDatos(string.Format("select IdUsuario, Rol, NombreUsuario from Usuarios where Activo = 1 and NombreUsuario  = '{0}' and Pass = '{1}'", Usuario, Pass));
                if (dt.Rows.Count > 0)
                {
                    this.IdUsuario = (int)dt.Rows[0]["IdUsuario"];
                    this.Rol = (int)dt.Rows[0]["Rol"];
                    this.NombreUsuario = dt.Rows[0]["NombreUsuario"].ToString();
                    retorno = true;
                }
            //}
            //catch (Exception)
            //{
            //    retorno = false;
            //}

            return retorno;
        }

        public bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select * from Usuarios where IdUsuario = " + IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdUsuario = (int)dtVentas.Rows[0]["IdUsuario"];
                    this.Nombres = dtVentas.Rows[0]["Nombres"].ToString();
                    this.Apellidos = dtVentas.Rows[0]["Apellidos"].ToString();
                    this.NombreUsuario = dtVentas.Rows[0]["NombreUsuario"].ToString();
                    this.Rol = (int)dtVentas.Rows[0]["Rol"];

                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Editar(int Id)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Usuarios set Nombres = '{0}', Apellidos = '{1}', NombreUsuario = '{2}', Rol = {3} where IdUsuario = {4}", this.Nombres, this.Apellidos, this.NombreUsuario, this.Rol, Id));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable Listado(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from Usuarios where " + Condicion + " " + ordenFinal);
        }
    }
}
