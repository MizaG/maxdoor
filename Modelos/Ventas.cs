﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Conexion;

namespace Modelos
{
    public class Ventas
    {
        public int IdVenta { get; set; }
        public string FechaYHora { get; set; }
        public int IdUsuario { get; set; }
        public int IdCliente { get; set; }
        public int IdTipoVenta { get; set; }
        public string Comentario { get; set; }
        public bool Completada { get; set; }
        public double MontoTotal { get; set; }
        public int CantidadCuotas { get; set; }
        public double Balance { get; set; }
        public bool Activo { get; set; }
        public List<Productos> productos { get; set; }
        public int IdProducto { get; set; }
        public double Cantidad { get; set; }
        public double Precio { get; set; }

        ConexionDB conexion = new ConexionDB();

        public Ventas()
        {
            this.IdVenta = 0;
            this.IdUsuario = 0;
            this.FechaYHora = "";
            this.MontoTotal = 0.0;
            this.IdProducto = 0;
            this.Cantidad = 0;
            this.Precio = 0.0;
            this.Activo = false;
            this.IdTipoVenta = 0;
            this.Comentario = "";
            this.Balance = 0;
            this.productos = new List<Productos>();
        }

        public void AgregarProductos(int IdProducto, double Cantidad, double Precio, double Importe)
        {
            this.productos.Add(new Productos(IdProducto, Cantidad, Precio, Importe));

        }

        public void LimpiarList()
        {
            this.productos.Clear();
        }

        public bool Insertar()
        {
            bool retorno = false;
            StringBuilder comando = new StringBuilder();
            //try
            //{
                retorno = conexion.Ejecutar(String.Format("Insert into Ventas (FechaYHora,IdUsuario,IdCliente,IdTipoVenta, Completada, MontoTotal,CantidadCuotas,Balance)" +
                    "Values (GETDATE(),{0},{1},{2},'{3}',{4},{5},{6});",
                                            this.IdUsuario, this.IdCliente, this.IdTipoVenta,  this.Completada, this.MontoTotal, this.CantidadCuotas,this.Balance));
                if (retorno)
                {
                    this.IdVenta = (int)conexion.ObtenerDatos(String.Format("select MAX(IdVenta) as IdVenta from Ventas")).Rows[0]["IdVenta"];
                    foreach (var pro in productos)
                    {
                        comando.AppendLine(String.Format("insert into VentasDetalle(IdProducto,IdVenta, Precio, Cantidad,Importe) values({0},{1},{2},{3},{4})", pro.IdProducto, this.IdVenta, pro.Precio, pro.Cantidad, pro.Importe));
                    }
                }

                retorno = conexion.Ejecutar(comando.ToString());
            //}
            //catch (Exception)
            //{
            //    retorno = false;
            //}

            return retorno;
        }

        public DataTable ListadoDetalle(int IdVenta)
        {
            ConexionDB con = new ConexionDB();
            
            return con.ObtenerDatos("select vd.Cantidad, p.Nombre, vd.Precio, vd.Importe from VentasDetalle vd inner join Productos p on vd.IdProducto = p.IdProducto where IdVenta = "+ IdVenta);
        }

        public bool Buscar(int IdBuscado)
        {
            DataTable dtVentas = new DataTable();
            DataTable dtVentaProductos = new DataTable();
            ConexionDB conexion = new ConexionDB();
            bool retorno = false;

            try
            {
                dtVentas = conexion.ObtenerDatos(string.Format("select IdVenta, FechaYHora, IdUsuario, IdCliente, IdTipoVenta, Completada, MontoTotal, CantidadCuotas, Balance from Ventas where Activo = 1 and Balance > 0 and IdVenta = {0}", IdBuscado));
                if (dtVentas.Rows.Count > 0)
                {
                    this.IdVenta = (int)dtVentas.Rows[0]["IdVenta"];
                    this.FechaYHora = dtVentas.Rows[0]["FechaYHora"].ToString();
                    this.IdUsuario = (int)dtVentas.Rows[0]["IdUsuario"];
                    this.IdCliente = (int)dtVentas.Rows[0]["IdCliente"];
                    this.IdTipoVenta = (int)dtVentas.Rows[0]["IdTipoVenta"];
                    this.Completada = (bool)dtVentas.Rows[0]["Completada"];
                    this.MontoTotal = (double)dtVentas.Rows[0]["MontoTotal"];
                    this.CantidadCuotas= (int)dtVentas.Rows[0]["CantidadCuotas"];
                    this.Balance = (double)dtVentas.Rows[0]["Balance"];

                    //dtVentaProductos = conexion.ObtenerDatos(String.Format("select p.ProductoId, p.Nombre,p.Precio, pvp.Cantidad, pvp.Importe from PreventaProductos pvp inner join Productos p on pvp.ProductoId = p.ProductoId where pvp.PreventaId = {0}", this.PreventaId));

                    //LimpiarList();
                    //foreach (DataRow row in dtVentaProductos.Rows)
                    //{
                    //    AgregarProductos((int)row["ProductoId"], (string)row["Nombre"], (double)row["Precio"], (int)row["Cantidad"], (double)row["Importe"]);
                    //}
                    retorno = true;
                }
            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public int MaximoId()
        {
            ConexionDB conexion = new ConexionDB();
            return (int)conexion.ObtenerDatos(String.Format("select MAX(IdVenta) as IdVenta from Ventas")).Rows[0]["IdVenta"];
        }

        public DataTable ListadoTipoVentas(string Campos, string Condicion, string Orden)
        {
            ConexionDB con = new ConexionDB();
            string ordenFinal = "";

            if (!Orden.Equals(""))
                ordenFinal = " Order by " + Orden;
            return con.ObtenerDatos("select " + Campos + " from TipoVentas where " + Condicion + " " + ordenFinal);
        }

        public bool CompletarVenta(int IdVenta)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set Completada = 1 where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable ListadoAnularVentas()
        {
            ConexionDB con = new ConexionDB();

            return con.ObtenerDatos("select v.IdVenta as ID, Convert(date, v.FechaYHora) as 'Fecha de Venta', c.Nombres +' '+ c.Apellidos as 'Nombre Cliente', v.MontoTotal as 'Monto de Venta', v.Balance from Ventas v inner join Clientes c on v.IdCliente = c.IdCliente  where v.Activo = 1 and Balance > 0");
        }

        public DataTable ListadoParaPagos()
        {
            ConexionDB con = new ConexionDB();
            
            return con.ObtenerDatos("select v.IdVenta as ID, Convert(date, v.FechaYHora) as 'Fecha de Venta', c.Nombres +' '+ c.Apellidos as 'Nombre Cliente', v.MontoTotal as 'Monto de Venta', v.Balance from Ventas v inner join Clientes c on v.IdCliente = c.IdCliente  where v.Activo = 1 and Balance > 0 and IdTipoVenta = 2");
        }

        public DataTable ListadoParaPagosFiltro(string Filtro)
        {
            ConexionDB con = new ConexionDB();

            return con.ObtenerDatos("select v.IdVenta as ID, Convert(date, v.FechaYHora) as 'Fecha de Venta', c.Nombres +' '+ c.Apellidos as 'Nombre Cliente', v.MontoTotal as 'Monto de Venta', v.Balance from Ventas v inner join Clientes c on v.IdCliente = c.IdCliente  where v.Activo = 1 and Balance > 0 and IdTipoVenta = 2 and (c.Nombres like '%"+ Filtro +"%' or c.Apellidos like '%"+ Filtro +"%')" );
        }

        public DataTable ListadoParaPagosFiltroFecha(string Filtro, string Desde, string Hasta)
        {
            ConexionDB con = new ConexionDB();

            return con.ObtenerDatos("select v.IdVenta as ID, Convert(date, v.FechaYHora) as 'Fecha de Venta', c.Nombres +' '+ c.Apellidos as 'Nombre Cliente', v.MontoTotal as 'Monto de Venta', v.Balance from Ventas v inner join Clientes c on v.IdCliente = c.IdCliente  where v.Activo = 1 and Balance > 0 and IdTipoVenta = 2 and (c.Nombres like '%" + Filtro + "%' or c.Apellidos like '%" + Filtro + "%' and Convert(date, FechaYHora) between '"+ Desde +"' and '"+ Hasta +"')");
        }

        public bool AnularVenta(int IdVenta)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update Ventas set Activo = 0 where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool AnularDetalleVenta(int IdVenta)
        {
            bool retorno = false;
            ConexionDB conexion = new ConexionDB();
            try
            {
                retorno = conexion.Ejecutar(String.Format("update VentasDetalle set Activo = 0 where IdVenta = {0}", IdVenta));

            }
            catch (Exception)
            {
                retorno = false;
            }

            return retorno;
        }

        public DataTable ListadoVentaDetalle(int Id)
        {
            ConexionDB con = new ConexionDB();

            return con.ObtenerDatos("select IdProducto, Cantidad from VentasDetalle where IdVenta = " + Id);
        }


    }
}
